#include <stdio.h>
#include <stdlib.h>
#include "TDAFila.h"

#define MAX 1000

AB A;

/***************************************************/
/*****************  CONSTRUCTORA  ******************/
/***************************************************/
int Posicion(int x, int Arreglo[MAX], int ini, int fin, int n)
{
    int i;
    i = ini;
    while (i <= fin)
    {
        if (Arreglo[i] == x)
            return i;
        i = i+1;
    }
    return i;
}
AB ReconstruyeRec(int IN[MAX], int iniIN, int finIN, int PRE[MAX], int iniPRE, int finPRE, int n)
{
    int raiz, pos;
    AB X = NULL;

    if (iniIN > finIN)
        return NULL;
    raiz = PRE[iniPRE];
    X = CreaNodoAB(raiz);
    pos = Posicion(raiz, IN, iniIN, finIN, n);

    X->izq = ReconstruyeRec(IN, iniIN, pos-1, PRE, iniPRE+1, iniPRE+pos-iniIN, n);
    X->der = ReconstruyeRec(IN, pos+1, finIN, PRE, iniPRE+pos-iniIN+1, finPRE, n);
    return X;
}
void IniciaReconstruye()
{
    int inOrden[MAX], preOrden[MAX], n, i;

    printf("\n\n\tIngrese el numero de nodos: ");
    scanf("%d", &n);
    i=1;
    printf("\n");
    while (i<=n) {
        printf("\tIngrese nodo %d, del recorrido en InOrden: ", i);
        scanf("%d", &inOrden[i]);
        i++;
    }
    i=1;
    printf("\n");
    while (i<=n) {
        printf("\tIngrese nodo %d, del recorrido en PreOrden: ", i);
        scanf("%d", &preOrden[i]);
        i++;
    }
    A = ReconstruyeRec(inOrden, 1, n, preOrden, 1, n, n);
    /*
    printf("\n\n\tRecorrido en PreOrden: ");
    PreOrden(A);
    MensajePausa();
    printf("\n\n\tRecorrido en InOrden: ");
    InOrden(A);
    MensajePausa();*/
}
/*****************************************************************/
/******************  ANALIZADORAS: RECORRIDOS  *******************/
/*****************************************************************/
void RecorridoPorNiveles()
{
    Fila F = NULL;
    AB auxAB;

    if (!(EsVacio(A))){
        F = InsertaFila(F,A);
        while (F != NULL){
            auxAB = PrimeroFila(F);
            F = SacaPrimeroFila(F);
            printf(" %-1d", auxAB->info);
            if (!EsVacio(Izq(auxAB)))
                F = InsertaFila(F,Izq(auxAB));
            if (!EsVacio(Der(auxAB)))
                F = InsertaFila(F,Der(auxAB));
        }
    }
}
void PreOrden(AB A)
{
    if (!EsVacio(A))
    {
        printf("%4d", A->info);
        PreOrden(Izq(A));
        PreOrden(Der(A));
    }
}
void InOrden(AB A)
{
    if (!EsVacio(A))
    {
        InOrden(Izq(A));
        printf("%4d", A->info);
        InOrden(Der(A));
    }
}
void PostOrden(AB A)
{
    if (!EsVacio(A))
    {
        PostOrden(Izq(A));
        PostOrden(Der(A));
        printf("%4d", A->info);
    }
}
void ImprimeElementosNivel(AB A, int nivel)
{
    if (EsVacio(A))
        printf(" -");
    else
    {
        if (nivel == 0)
            printf(" %2d", A->info);
        else
        {
            ImprimeElementosNivel(A->izq, nivel-1);
            printf(" |");
            ImprimeElementosNivel(A->der, nivel-1);
        }
    }
 }
/***************************************************/
/****************  ANALIZADORAS  *******************/
/***************************************************/
int Peso(AB A)
{
    if (EsVacio(A))
        return 0;
    else
        return 1 + Peso(Izq(A)) + Peso(Der(A));
}
BOOL Busca(AB A, int x)
{
    if  (EsVacio(A))
        return FALSO;
    if (A->info == x)
        return VERDADERO;
    else
        return Busca(Izq(A), x) || Busca(Der(A), x);
}
int NumHojas(AB A)
{
    if  (EsVacio(A))
        return 0;
    if (EsHoja(A))
        return 1;
    else
        return NumHojas(Izq(A)) + NumHojas(Der(A));
}
int Altura(AB A)
{
    if (EsVacio(A))
        return 0;
    else
        return Maximo(Altura(Izq(A)), Altura(Der(A))) + 1;
}
int MayorAB(AB A, int mayor)
{
    int mayorIzq, mayorDer;

    if (EsVacio(A))
        return mayor;
    else
    {
        if (A->info > mayor)
            mayor = A->info;
        mayorIzq = MayorAB(A->izq, mayor);
        mayorDer = MayorAB(A->der, mayorIzq);
        return mayorDer;
    }

}
int Mayor(AB A)
{
    int mayor, mayorIzq, mayorDer;

    if (EsHoja(A))
        return A->info;
    else
    {
        mayor = A->info;
        if (!EsVacio( A->izq))
            mayorIzq = Mayor(A->izq);
        if (!EsVacio( A->der))
            mayorDer = Mayor(A->der);

        if (mayor >= mayorIzq && mayor >= mayorDer)
            return mayor;
        if (mayorIzq >= mayor && mayorDer >= mayorDer)
            return mayorIzq;
        else
            return mayorDer;
    }
}

/***************************************************/
/***************   DESTRUCTORA   *******************/
/***************************************************/
void Destruye(AB A)
{
    AB auxI, auxD;
    if (!EsVacio(A))
    {
        auxI = Izq(A);
        auxD = Der(A);
        A->izq = NULL;
        A->der = NULL;
        free(A);
        Destruye(auxI);
        Destruye(auxD);
    }
    A = NULL;
}

/***************************************************/
/***********   OTRAS FUNCIONES   *******************/
/***************************************************/

int ImprimeMenu(void)
{
    int opcion;

    system("cls");
    printf("\n\t\t\tMENU");
    printf("\n\t\t\t====\n");
    printf("\n\t\t1. Recorrido por Niveles.");
    printf("\n\t\t2. Recorrido PreOrden.");
    printf("\n\t\t3. Recorrido InOrden.");
    printf("\n\t\t4. Recorrido PostOrden.");
    printf("\n\t\t5. Peso.");
    printf("\n\t\t6. Buscar.");
    printf("\n\t\t7. Reconstruye AB (PreOrden - InOrden).");
    printf("\n\t\t8. Altura.");
    printf("\n\t\t9. Mayor elemento.");
    printf("\n\t\t10. Imprime AB.");
    printf("\n\t\t20. Salir.");
    printf("\n\n\t\tIngrese una opci%cn: ", 162);
    scanf("%d", &opcion);
    return opcion;
}
void ProcesaOpciones(int opcion)
{
    int peso, valor, h, i, mayor;
    BOOL resp;

    switch(opcion)
    {
        case 1:
            printf("\n\n\tRecorrido por Niveles: ");
            RecorridoPorNiveles();
            MensajePausa();
            break;
        case 2:
            printf("\n\n\tRecorrido en PreOrden: ");
            PreOrden(A);
            MensajePausa();
            break;
        case 3:
            printf("\n\n\tRecorrido en InOrden: ");
            InOrden(A);
            MensajePausa();
            break;
        case 4:
            printf("\n\n\tRecorrido en PostOrden: ");
            PostOrden(A);
            MensajePausa();
            break;
        case 5:
            peso = Peso(A);
            printf("\n\n\tEl total de nodos es: %d", peso);
            MensajePausa();
            break;
        case 6:
            printf("\n\n\tIngrese valor a buscar: ");
            scanf("%d", &valor);
            resp = Busca(A, valor);
            if (resp)
                printf("\n\n\tEl valor %d SI est%c.", valor, 160);
            else
                printf("\n\n\tEl valor %d NO est%c.", valor, 160);
            MensajePausa();
            break;
        case 7:
            Destruye(A);
            IniciaReconstruye();
            break;
        case 8:
            //h = Altura_Alumnos(A);
            h = Altura(A);
            printf("\n\n\tLa altura del AB es: %d", h);
            MensajePausa();
            break;
        case 9:
            if (EsVacio(A))
                printf("\n\n\tArbol vac�o");
            else
            {
                mayor = Mayor(A);
                printf("\n\n\tEl mayor es %d ", mayor);
            }
            MensajePausa();
            break;
        case 10:
            h = Altura(A);
            printf("\n");
            for(i=0;i<h;i++)
            {
                printf("\n\t\t");
                ImprimeElementosNivel(A,i);
            }

            MensajePausa();
            break;
        case 20:
            break;
        default:
            printf("\n\n\tERROR: Ingrese una opci%cn v%clida.", 162, 160);
            MensajePausa();
    }
}
void Menu()
{
    int opcion = 0;

    while (opcion != 20)
    {
        opcion = ImprimeMenu();
        ProcesaOpciones(opcion);
    }
}
int main()
{
    A = InicializaAB();
    A = CreaNodoAB(100);
    A->izq = CreaNodoAB(82);
    A->der = CreaNodoAB(35);
    A->izq->izq = CreaNodoAB(40);
    A->izq->der = CreaNodoAB(1);
    A->izq->der->izq = CreaNodoAB(9);
    A->izq->der->izq->der = CreaNodoAB(70);

    Menu();

    printf("\n\n");
    return 0;
}
