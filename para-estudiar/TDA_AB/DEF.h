#define VERDADERO 1
#define FALSO 0

/*Tipo de Dato �rbol Binario:
    Estructura y tipo de dato de un nodo de un �rbol Binario (tNodoAB),
    y tipo de dato para un puntero a un nodo �rbol Binario (AB).*/

struct NodoAB{
    int info; /* <-- Llamar estructura con los datos del nodo */
    struct NodoAB *izq;
    struct NodoAB *der;
};
typedef struct NodoAB tNodoAB;
typedef tNodoAB *AB;

/*Tipo de Dato Fila:
    Estructura y tipo de dato de un nodo de una Fila (tNodoFila),
    y tipo de dato para un puntero a un nodo Fila (Fila).*/

struct NodoFila{
    AB info;
    struct NodoFila *sig;
};
typedef struct NodoFila tNodoFila;
typedef tNodoFila *Fila;

/*Tipo de Dato Booleano*/
typedef int BOOL;
