#include "DEF.h"

/*Operaciones de Abstracción sobre AB: */
AB CreaNodoAB(int valor);
AB InicializaAB();
BOOL EsVacio(AB A);
BOOL EsHoja(AB A);
AB Izq(AB A);
AB Der(AB A);

/*Operaciones sobre Fila: */
Fila CreaNodoFila(AB ptro);
Fila InsertaFila(Fila F, AB ptro);
AB PrimeroFila(Fila F);
Fila SacaPrimeroFila(Fila F);

/*Operaciones Complementarias*/
int Maximo(int a, int b);
void MensajePausa(void);

/**************************************************/
/*******  Declaración de Funciones sobre AB *******/
/**************************************************/
AB CreaNodoAB(int valor)
{
    AB aux;

    aux = (AB)malloc(sizeof(tNodoAB));
    if (aux != NULL)
    {
        aux->info = valor;
        aux->izq = NULL;
        aux->der = NULL;
    }
    else
    {
        system("cls");
        printf("\n\tERROR: No hay memoria suficiente para generar un nuevo Nodo.");
        printf("\n\tEste programa se cerrar%c.", 160);
        exit(1);
    }
    return aux;
}
BOOL EsVacio(AB A)
{
    if (A == NULL)
        return VERDADERO;
    else
        return FALSO;
}
BOOL EsHoja(AB A)
{
    if (A == NULL)
        return FALSO;
    if (Izq(A) == NULL && Der(A) == NULL)
        return VERDADERO;
    else
        return FALSO;
}
AB Izq(AB A)
{
    return A->izq;
}
AB Der(AB A)
{
    return A->der;
}
AB InicializaAB()
{
    return NULL;
}
/****************************************************/
/******  Declaramación de Funciones sobre Fila ******/
/****************************************************/
Fila CreaNodoFila(AB ptro)
{
    Fila aux;

    aux = (Fila)malloc(sizeof(tNodoFila));
    if (aux != NULL)
    {
        aux->info = ptro;
        aux->sig = NULL;
    }
    else
    {
        system("cls");
        printf("\n\tERROR: No hay memoria suficiente para generar un nuevo Nodo.");
        printf("\n\tEste programa se cerrar%c.", 160);
        exit(1);
    }
    return aux;
}
Fila InsertaFila(Fila F, AB ptro)
{
    Fila aux, pNodo = CreaNodoFila(ptro);

    if (F == NULL)
        F = pNodo;
    else
    {
        aux = F;
        while (aux->sig != NULL)
            aux = aux->sig;
        aux->sig = pNodo;
    }
    pNodo = NULL;
    return F;
}
AB PrimeroFila(Fila F)
{
    return F->info;
}
Fila SacaPrimeroFila(Fila F)
{
    Fila aux;
    if (F!=NULL)
    {
        aux = F;
        F = F->sig;
        aux->sig = NULL;
        free(aux);
    }
    return F;
}
/****************************************************/
/**** Declaramación de Funciones Complementarias ****/
/****************************************************/
int Maximo(int a, int b)
{
    if (a>b)
        return a;
    else
        return b;
}
void MensajePausa(void)
{
    printf("\n\n\n\t");
    system("pause");
}

