#include <stdio.h>
#include <stdlib.h>

struct Nodo {
    int info;              /* información */
    struct Nodo *izq;
    struct Nodo *der;
};
typedef struct Nodo tNodoAB;
typedef tNodoAB *AB;

AB CreaNodoAB(int valor)
{
    AB aux;

    aux = (AB)malloc(sizeof(tNodoAB));
    if (aux != NULL)
    {
        aux->info = valor;
        aux->izq = NULL;
        aux->der = NULL;
    }
    else
    {
        system("cls");
        printf("\n\tERROR: No hay memoria suficiente para generar un nuevo Nodo.");
        printf("\n\tEste programa se cerrar%c.", 160);
        exit(1);
    }
    return aux;
}

void integrar_datos(int n, AB raiz){
  AB ramita;
  int d;
  for (int i; i <=n; i++){
    scanf("%d\n", d);
    if (raiz == NULL){
      if (d < raiz -> info){
        raiz -> izq = ramita;
      }
      else {
        raiz -> der = ramita;
      }
      ramita -> info = d;
    }
    else{
      raiz = ramita;
    }
  }
}

int main(int argc, char const *argv[]) {
  AB arbolito = CreaNodoAB(5);
  printf("\nCantidad de nodos:\n");
  int numero_nodos;
  scanf("%d\n", numero_nodos );
  integrar_datos(numero_nodos,arbolito);
  printf("shao\n");
  return 0;
}
