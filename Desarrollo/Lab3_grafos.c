#include <stdio.h>
#include <stdlib.h>
#define inf 30000


/************************************************/
/*				LISTADO DE FUNCIONES			*/
/************************************************/

/*Estructura Matrices Dinamicas*/
char **MatrizDinamicaCaracter(int m, int n);
int **MatrizDinamicaNumero(int m, int n);
int *ArregloDinamico(int n);
/*Impresion Matrices Dinamicas*/
void ImprimeMatrizCaracter(char **Matriz, int M, int N);
void ImprimeMatrizNumerica(int **Matriz, int M, int N);
void ImprimeMatrizAdyacencia(int **Matriz, int P);
void ImprimeArreglo(int *Arreglo);
/*Funciones para liberar espacio*/
void LiberarMatrizCaracter (char **Matriz, int m);
void LiberarMatrizNumerica (int **Matriz, int m);
/*Funciones de creacion y busqueda*/
int **MatrizCero(int m, int n);
int MaximoMatriz (int **Matriz, int m, int n);
/*Funciones de archivo de texto*/
void RevisionArchivo (FILE *Archivo);
FILE *ObtenerTexto(char Nombre[]);
int *Dimensiones(FILE *Archivo);
void LlenaMatrizDesdeArchivo(char **Matriz, int m, int n, FILE *pArchivo);
/*Funciones de verificacion*/
int EstLab(char c);
void TieneEntrada(char **Matriz, int n);
void TieneSalida(char **Matriz, int m, int n);
void EsLaberinto (char **Matriz, int m, int n);
/*Funciones de Lectura y transformacion*/
char **ObtenerLaberinto(FILE *Archivo, int m, int n);
int **LaberintoNumerico(char **Lab, int m, int n);
int *OrdenarArreglo(int *Arreglo);
/*Funciones de Adyacencia*/
int **ConectarPasillos(int **Lab, int **Ady, int i, int j);
int **MatrizAdyacencia(int **Matriz, int m, int n);
/*Funciones de Dijsktra*/
int *DIJKSTRA(int **Grafo, int n, int v0, int buscado);
int *CaminoMasCorto(int *NodoAnterior, int n, int v0, int buscado);
int NumeroNodosSinVisitar(int *Visitados, int n);
int NodoMinimoCostoSinVisitar(int *Visitados, int *Costo, int n);
/*Funciones programa principal*/
void ImprimirResultados(char **Lab, int **LabInt, int **Ady, char **LabFinal, int *Camino, int m, int n);
void LiberarResultados(char **Lab, int **LabInt, int **Ady, int m, int n);
int clrscr();
int **FuncionesPrincipales(char **Lab, int m, int n);

/****************************************************************************************************************************/
/*												INICIO PROGRAMA PRINCIPAL													*/
/****************************************************************************************************************************/

int main(){
    FILE *pArchivo;
    char **Lab=NULL;
    int *dim=NULL;
	pArchivo=ObtenerTexto("datos.txt");
	dim=Dimensiones(pArchivo);
	Lab=ObtenerLaberinto(pArchivo, dim[1], dim[2]);
    fclose(pArchivo);
    FuncionesPrincipales(Lab, dim[1], dim[2]);
    return 0;
}

/****************************************************************************************************************************/
/*												FIN PROGRAMA PRINCIPAL													*/
/****************************************************************************************************************************/

/********************************************************************/
/*					FUNCIONES DE MATRICES DINAMICAS					*/
/********************************************************************/

/****************************************************/
/*			Estructura Matrices Dinamicas			*/
/****************************************************/

/*Crea matriz dinamica de caracteres de dimensiones m x n*/

char **MatrizDinamicaCaracter(int m, int n){
	int i;
	char **Matriz;
	Matriz = (char **)malloc(m*sizeof(char *));
    for(i=0;i<m;i++) {
        Matriz[i] = (char *)malloc(n*sizeof(char));
   }
   return Matriz;
}

/*Crea matriz dinamica de numeros de dimensiones m x n*/

int **MatrizDinamicaNumero(int m, int n){
	int i;
	int **Matriz;
	Matriz = (int **)malloc(m*sizeof(int *));
    for(i=0;i<m;i++) {
        Matriz[i] = (int *)malloc(n*sizeof(int));
   }
   return Matriz;
}

/*Crea arreglo dinamico de numeros de dimension n*/

int *ArregloDinamico(int n){
    int *aux;
    int i;
    aux = (int *)malloc(n*(sizeof(int)));
    if (!aux){
        printf("\nNo hay memoria suficiente.");
        exit(1);
    }
    for(i=0;i<n;i++)
        aux[i] = 0;
    return aux;
}

/****************************************************/
/*			Impresion Matrices Dinamicas			*/
/****************************************************/

/*Imprime matriz dinamica de caracteres de dimensiones M x N*/

void ImprimeMatrizCaracter(char **Matriz, int M, int N){
    int i, j;
    for(i=0;i<M;i++){
        for(j=0;j<N;j++)
        	printf("%c", Matriz[i][j]);
        printf("\n");
    }
}

/*Imprime Matriz dinamica de numeros de dimensiones M x N*/

void ImprimeMatrizNumerica(int **Matriz, int M, int N){
    int i, j;
    for(i=0;i<M;i++){
        for(j=0;j<N;j++)
        	printf("%d\t", Matriz[i][j]);
        printf("\n");
    }
}

/*Imprime Matriz de Adyacencia*/

void ImprimeMatrizAdyacencia(int **Matriz, int P){
    int i, j;
    for(i=0;i<P;i++){
        for(j=0;j<P;j++)
        	printf("%d ", Matriz[i][j]);
        printf("\n");
    }
}

/*Imprime Arreglo Dinamico*/

void ImprimeArreglo(int *Arreglo){
    int i;
    for(i=1;i<=Arreglo[0];i++){
        printf("%d ", Arreglo[i]);
    }
}

/*Libera espacio en la memoria ocupada por las matrices dinamicas*/

void LiberarMatrizCaracter (char **Matriz, int m){
	int i;
	for(i=0;i<m;i++)
		free(Matriz[i]);
    free(Matriz);
}

void LiberarMatrizNumerica (int **Matriz, int m){
	int i;
	for(i=0;i<m;i++)
		free(Matriz[i]);
    free(Matriz);
}

/********************************************************/
/*			Funciones de creacion y busqueda			*/
/********************************************************/

/*Crea matriz dinamica de numeros de dimensiones m x n, rellena de ceros*/

int **MatrizCero(int m, int n){
	int **Ceros, i, j;
	Ceros=MatrizDinamicaNumero(m, n);
	for (i=0;i<m;i++){
		for (j=0;j<n;j++)
			Ceros[i][j]=0;
	}
	return Ceros;
}

/*Buscar el valor maximo de una matriz dinamica*/

int MaximoMatriz (int **Matriz, int m, int n){
	int max=0, i, j;
	for (i=0;i<m;i++){
		for (j=0;j<n;j++){
			if (Matriz[i][j]>max)
				max=Matriz[i][j];
		}
	}
	return max;
}

/********************************************************************/
/*					FUNCIONES DE ARCHIVO DE TEXTO					*/
/********************************************************************/

/*Revisa si el archivo de texto leido existe*/

void RevisionArchivo (FILE *Archivo){
	if (Archivo==NULL){
		printf("\n\nNo se pudo abrir el archivo.\n\n");
		printf("El archivo debe estar en la misma ubicacion que el ejecutable y llamarse datos.txt\n\n.");
		exit(1);
	}
}

/*Abre archivo de texto desde la ubicacion entregada*/

FILE *ObtenerTexto(char Nombre[]){
	FILE *Archivo;
	Archivo = fopen(Nombre, "r");
	RevisionArchivo(Archivo);
	return Archivo;
}

/*Lee el texto abierto y copia las dimensiones del laberinto a un arreglo dinamico*/

int *Dimensiones(FILE *Archivo){
	int *dim=NULL;
	dim=ArregloDinamico(2);
	fscanf(Archivo, "%d", &dim[1]);
	fscanf(Archivo, "%d", &dim[2]);
	return dim;
}

/*Lee el texto abierto y copia el laberinto a una matriz dinamica*/

void LlenaMatrizDesdeArchivo(char **Matriz, int m, int n, FILE *pArchivo){
    int i, j;
    char c;
    fscanf(pArchivo, "%c", &c);
    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
           fscanf(pArchivo, "%c", &Matriz[i][j]);
    	}
    	fscanf(pArchivo, "%c", &c);
	}
}

/************************************************************/
/*					FUNCIONES DE LABERINTO					*/
/************************************************************/

/************************************************/
/*			Funciones de verificacion			*/
/************************************************/

/*Verifica si el caracter leido corresponde a la estructura de laberinto*/

int EstLab(char c){
	if (c=='S' || c=='E' || c==' ' || c=='#')
		return 1;
	else
		return 0;
}

/*Verifica si el laberinto tiene una sola entrada*/

void TieneEntrada(char **Matriz, int n){
	int j, cont=0;
	for (j=1;j<(n-1);j++){
		if (Matriz[0][j]=='E')
			cont=cont+1;
	}
	if (cont==0){
		printf("\n\nEl laberinto entregado no tiene entrada.\n");
		exit(1);
	}
	if (cont>1){
		printf("\n\nEl laberinto entregado tiene mas de una entrada.\n");
		exit(1);
	}
}

/*Verifica si el laberinto tiene una sola salida*/

void TieneSalida(char **Matriz, int m, int n){
	int j, cont=0;
	for (j=1;j<(n-1);j++){
		if (Matriz[m-1][j]=='S')
			cont=cont+1;
	}
	if (cont==0){
		printf("\n\nEl laberinto entregado no tiene salida.\n");
		exit(1);
	}
	if (cont>1){
		printf("\n\nEl laberinto entregado tiene mas de una salida.\n");
		exit(1);
	}
}

/*Verifica si el texto leido corresponde a un laberinto, de no ser asi termina el programa*/

void EsLaberinto (char **Matriz, int m, int n){
	int i, j, entrada, salida;
	for (i=0;i<m;i++){
		for (j=0;j<n;j++){
			if (!EstLab(Matriz[i][j])){
				printf("\n\nEl laberinto entregado no posee la estructura definida.\n");
				printf("Este debe estar compuesto solo por los caracteres: E, S, # y espacio.\n");
				printf("Favor revisar que esto se cumpla.\n\n");
				exit(1);
			}
		}
	}
	TieneEntrada(Matriz, n);
	TieneSalida(Matriz, m, n);
}

/************************************************************/
/*			Funciones de Lectura y transformacion			*/
/************************************************************/

/*Guarda en una matriz dinamica el texto leido y verifica si es un laberinto*/

char **ObtenerLaberinto(FILE *Archivo, int m, int n){
	char **Matriz=NULL;
	Matriz=MatrizDinamicaCaracter(m, n);
    LlenaMatrizDesdeArchivo(Matriz, m, n, Archivo);
    EsLaberinto(Matriz, m, n);
    return Matriz;
}

/*Transforma el laberinto de caracteres a uno numerico, asignando un numero a la entrda, salida y pasillos*/

int **LaberintoNumerico(char **Lab, int m, int n){
	int **LabInt=NULL;
	int i, j, cont=1;
	LabInt=MatrizDinamicaNumero(m,n);
	for (i=0;i<m;i++){
		for (j=0;j<n;j++){
			if (Lab[i][j]=='#')
				LabInt[i][j]=0;
			else{
				LabInt[i][j]=cont;
				cont=cont+1;
			}
		}
	}
	return LabInt;
}

/*Ordena un Arreglo Dinamico*/

int *OrdenarArreglo(int *Arreglo){
	int *Orden, aux, i, j;
	Orden=ArregloDinamico(Arreglo[0]);
	for (i=0;i<=Arreglo[0];i++)
		Orden[i]=Arreglo[i];
	for (i=1; i<Orden[0]; i++){ 
		for (j=i+1; j<=Orden[0]; j++){ 
			if (Orden[j] < Orden[i]){ 
        		aux = Orden[j]; 
        		Orden[j] = Orden[i]; 
        		Orden[i] = aux; 
    		} 
    	} 
  	}
	return Orden; 
}

/********************************************/
/*			Funciones de Adyacencia			*/
/********************************************/

/*En caso de que el valor observado sea un pasillo, asigna uno a los valores cercanos a este en la matriz de adyacencia*/

int **ConectarPasillos(int **Lab, int **Ady, int i, int j){
	if (Lab[i-1][j]>0){
		Ady[Lab[i-1][j]-1][Lab[i][j]-1]=1;
		Ady[Lab[i][j]-1][Lab[i-1][j]-1]=1;
	}
	if (Lab[i][j+1]>0){
		Ady[Lab[i][j+1]-1][Lab[i][j]-1]=1;
		Ady[Lab[i][j]-1][Lab[i][j+1]-1]=1;
	}
	if (Lab[i+1][j]>0){
		Ady[Lab[i+1][j]-1][Lab[i][j]-1]=1;
		Ady[Lab[i][j]-1][Lab[i+1][j]-1]=1;
	}
	if (Lab[i][j-1]>0){
		Ady[Lab[i][j-1]-1][Lab[i][j]-1]=1;
		Ady[Lab[i][j]-1][Lab[i][j-1]-1]=1;
	}
	return Ady;
}

/*Crea la matriz de adyacencia*/

int **MatrizAdyacencia(int **Matriz, int m, int n){
	int **Ady, max, i, j;
	max=MaximoMatriz(Matriz, m, n);
	Ady=MatrizCero(max, max);
	for (i=1;i<(m-1);i++){
		for (j=1;j<(n-1);j++){
			if (Matriz[i][j]>0){
				Ady=ConectarPasillos(Matriz, Ady, i, j);
			}
		}
	}
	return Ady;
}

/********************************************************/
/*					FUNCIONES DIJKSTRA					*/
/********************************************************/

/* Aplicacion de Dijkstra */
int *DIJKSTRA(int **Grafo, int n, int v0, int buscado){
	int *Visitados, *Costo, *NodoAnterior, *Camino;
    int i, j, w=v0;
    Visitados = ArregloDinamico(n);
    Costo = ArregloDinamico(n);
    NodoAnterior = ArregloDinamico(n);
    Visitados[v0] = 1;
    for(i=0;i<n;i++){
        if (Grafo[v0][i] == 0)
            Costo[i] = inf;
        else
            Costo[i] = Grafo[v0][i];
        NodoAnterior[i] = v0;
    }
    while (NumeroNodosSinVisitar(Visitados, n)>1){
        w = NodoMinimoCostoSinVisitar(Visitados, Costo, n);
        Visitados[w] = 1;
        for(j=0;j<n;j++)
        {
            if ((Grafo[w][j] != 0) && (!Visitados[j]))
            {
                if (Costo[w] + Grafo[w][j] < Costo[j])
                {
                    NodoAnterior[j] = w;
                    Costo[j] = Costo[w] + Grafo[w][j];
                }
            }
        }
    }
    if (Costo[buscado-1]==30000){
    	printf("No existe camino desde la Entrada hasta la Salida.");
    	exit(1);
	}
	Camino=ArregloDinamico(n);
    Camino=CaminoMasCorto(NodoAnterior, n, 0, buscado-1);
    return Camino;
}

/*Devuelve arreglo dinamico con el camino mas corto*/
/*El primer elemento corresponde a la dimension del arreglo*/

int *CaminoMasCorto(int *NodoAnterior, int n, int v0, int buscado){
    int *CaminoAux, *Camino, i, j, nodo, cont=1;
    CaminoAux = ArregloDinamico(n);
    for(i=0;i<n;i++){
        if (i == buscado){
            j = 0;
            CaminoAux[j] = i;
            j++;
            nodo = NodoAnterior[i];
            while(nodo != v0){
                CaminoAux[j] = nodo;
                j++;
                nodo = NodoAnterior[nodo];
            }
            CaminoAux[j] = nodo;
            Camino = ArregloDinamico(j+1);
        	Camino[0] = j+1;
        	while (j>=0){
        		Camino[cont]=CaminoAux[j]+1;
        		j--;
        		cont++;
			}
        }
    }
    return Camino;
}

/*Devuelve el numero de nodos que no se han visitado*/

int NumeroNodosSinVisitar(int *Visitados, int n){
    int i, cont;
    i = 0;
    cont = 0;
    while (i < n){
        if (!Visitados[i]){
            cont++;
        }
        i++;
    }
   return cont;
}

/*Nodos de minimo costo sin visitar*/

int NodoMinimoCostoSinVisitar(int *Visitados, int *Costo, int n){
    int i, nodo, minimo;
    i = 0;
    while (Visitados[i]){
        i++;
    }
    minimo = Costo[i];
    nodo = i;
    i++;
    while(i < n){
        if (!Visitados[i]){
            if (Costo[i] < minimo){
                minimo = Costo[i];
                nodo = i;
            }
        }
        i++;
    }
    return nodo;
}

/*Por medio del Laberinto leido del texto, del representado en numeros enteros y el camino*/
/*se obtiene el laberinto final, que muestra el camino mas corto con puntos*/

char **LaberintoFinal(char **Lab, int **LabInt, int *Camino, int m, int n){
	int i, j, cont=1;
	int *COrdenado;
	COrdenado=OrdenarArreglo(Camino);
	char **LC;
	LC=MatrizDinamicaCaracter(m, n);
	for (i=0;i<m;i++){
		for (j=0;j<n;j++){
			LC[i][j]=Lab[i][j];
		}
	}
	for (i=0;i<m;i++){
		for (j=0;j<n;j++){
			if (LabInt[i][j]==COrdenado[cont] && cont<=COrdenado[0]){
				if (LC[i][j]==' ')
					LC[i][j]='.';
				cont++;
			}
		}
	}
	return LC;
}

/********************************************************************/
/*					FUNCIONES PROGRAMA PRINCIPAL					*/
/********************************************************************/

/*Imprimir matrices*/

void ImprimirResultados(char **Lab, int **LabInt, int **Ady, char **LabFinal, int *Camino, int m, int n){
	int max;
	max=MaximoMatriz(LabInt, m, n);
	printf("Laberinto leido desde archivo de texto.\n\n");
	ImprimeMatrizCaracter(Lab, m, n);
	printf("\n\n");
	clrscr();
	printf("Laberinto representado como una secuencia numerica.\n");
	printf("(0: Muro, Numeros>0: Pasillos, Minimo: Entrada, Maximo: Salida.)\n\n");
	ImprimeMatrizNumerica(LabInt, m, n);
	printf("\n\n");
	clrscr();
	printf("Matriz de adyacencia.\n\n");
	ImprimeMatrizAdyacencia(Ady, max);
	printf("\n\n");
	clrscr();
	printf("Camino entre E(1) y S(%d).\n\n",max);
	ImprimeArreglo(Camino);
	printf("\n\n");
	clrscr();
	printf("La Matriz Final corresponde a:\n\n");
	ImprimeMatrizCaracter(LabFinal, m, n);
	printf("\n\n");
	clrscr();
}

/*Libera el espacio usado por todas las matrices*/

void LiberarResultados(char **Lab, int **LabInt, int **Ady, int m, int n){
	int max;
	max=MaximoMatriz(LabInt, m, n);
	LiberarMatrizCaracter(Lab, m);
	LiberarMatrizNumerica(LabInt, m);
	LiberarMatrizNumerica(Ady, max);
}

/*Pausa pantalla*/

int clrscr(){
    system("pause");
    system("@cls||clear");
}

/*Modificaciones y creaciones de matrices*/

int **FuncionesPrincipales(char **Lab, int m, int n){
	int **LabInt=NULL, **Ady=NULL, *Camino=NULL, *costo=NULL, max;
	char **LabFinal;
	LabInt=LaberintoNumerico(Lab, m, n);
	max=MaximoMatriz(LabInt, m, n);
    Ady=MatrizAdyacencia(LabInt, m, n);
    Camino=DIJKSTRA(Ady, max,0,max);
    LabFinal=LaberintoFinal(Lab, LabInt, Camino, m, n);
    ImprimirResultados(Lab, LabInt, Ady, LabFinal, Camino, m, n);
    LiberarResultados(Lab, LabInt, Ady, m, n);
}
