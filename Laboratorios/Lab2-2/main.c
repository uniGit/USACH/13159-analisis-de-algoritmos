#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct nodo {
  // estructura de la lista de enlace
  int dato;
  struct nodo *sig;
};
typedef struct nodo *LLS;

void inicializarLista(LLS *lista, int d, LLS sig) {
  // funcion que inicializa un nodo de una lista de enlace
  *lista = malloc(sizeof(struct nodo));
  (*lista)->dato = d;
  (*lista)->sig = sig;
}

// variable global que indica el numero de nodos del grafo;
int n = 7;

void borrarLista(LLS *lista) {
  // algoritmo de borrado de una lista de enlace simple
  // deja la lista como NULL (por esto la doble indireccion)
  if (*lista == NULL) return;
  LLS aux;
  while (*lista != NULL) {
    aux = (*lista)->sig;
    free(*lista);
    *lista = aux;
  }
  *lista = NULL;
}

char busquedaProfundidad(int *grafo, int nodo, int fin, char *visitado,
                         LLS *recorrido) {
  // algoritmo de busqueda por profundidad (depth first search)
  // este algoritmo genera una lista de enlaces con el recorrido siempre  y
  // cuando se encuentre tal recorrido
  visitado[nodo] = 1;
  if (nodo == fin) {
    // caso termino, el nodo i es el nodo de salida
    inicializarLista(recorrido, nodo, *recorrido);
    return 1;
  }
  int anexo;
  for (anexo = 0; anexo < n; anexo++) {
    if (visitado[anexo] == 0 && grafo[nodo * n + anexo] != 0)
      // si se encuentra un camino a partir del nodo i que sea valido entonces
      // el algoritmo termina
      if (busquedaProfundidad(grafo, anexo, fin, visitado, recorrido)) {
        inicializarLista(recorrido, nodo, *recorrido);
        return 1;
      }
  }
  // en caso de no exisir camino se retorna 0 (False)
  return 0;
}

int flujoCamino(int *grafo, LLS recorrido) {
  // esta funcion permite conocer la menor conexion existente en un recorrido
  int minimo = INT_MAX;
  if (recorrido == NULL) return 0;
  while (recorrido->sig != NULL) {
    if (grafo[recorrido->dato * n + recorrido->sig->dato] < minimo)
      minimo = grafo[recorrido->dato * n + recorrido->sig->dato];
    recorrido = recorrido->sig;
  }
  return minimo;
}

void actualizarGrafo(int *grafo, LLS recorrido, int flujo) {
  // funcion que modifica los valores de las conexiones de un cierto recorrido
  // dentro del grafo cuando se realiza un decremento de igual manera se
  // incrementa la conexion contraria
  if (recorrido == NULL) return;
  while (recorrido->sig != NULL) {
    grafo[recorrido->dato * n + recorrido->sig->dato] -= flujo;
    grafo[recorrido->sig->dato * n + recorrido->dato] += flujo;
    recorrido = recorrido->sig;
  }
}

void imprimierCamino(LLS recorrido, int flujoRecorrido) {
  printf("recorrido: ");
  LLS aux = recorrido;
  while (aux != NULL) {
    // se aplica el offset del grafo a los computadores (el pc 1 corresponde
    // al nodo 0)
    printf("%d ", aux->dato + 1);
    aux = aux->sig;
  }
  printf("flujo: %d\n", flujoRecorrido);
}

int flujoMaximo(int *grafo, int nodo, int fin, char *visitado, char mostrar) {
  // esta funcion permite obtener el maximo flujo entre dos nodos de un grafo
  LLS recorrido = NULL;
  // inicialmente ningun nodo ha sido visitado
  int m;
  for (m = 0; m < n; m++) {
    visitado[m] = 0;
  }
  // algoritmo de busqueda por profndudad -> se obtiene un recorrido valido
  busquedaProfundidad(grafo, nodo, fin, visitado, &recorrido);
  // obtiene el mayor flujo posible para este recorrido
  int flujoRecorrido = flujoCamino(grafo, recorrido);
  // se modician las conexiones pertenecietnes al recorrido utilizando el maximo
  // flujo permitido
  actualizarGrafo(grafo, recorrido, flujoRecorrido);
  // se limpia la lista y se retorna el maximo flujo permitido para el recorrido
  if (mostrar == 1 && flujoRecorrido != 0) {
    imprimierCamino(recorrido, flujoRecorrido);
  }
  borrarLista(&recorrido);
  return flujoRecorrido;
}

int parseStringInt(char **str) {
  // funcion que lee un string y entrega un numero asociado, avanza el puntero
  // dentro del string
  int suma = 0;
  while (**str != ' ' && **str != '\0') {
    suma = suma * 10 + **str - '0';
    *str = *str + 1;
  }
  *str = *str + 1;
  return suma;
}

void entradaArchivos(int *inicio, int *termino, int **grafo) {
  // lectura de archivo
  FILE *entrada;
  entrada = fopen("Entrada.in", "r");
  char linea[1000];
  if (entrada == NULL) {
    printf("archivo no encontrado\n");
    *grafo = NULL;
    return;
  }

  printf(
      "Ingrese cantidad de computadores, computador de inicio y termino "
      "separados por espacio\n");
  scanf("%d %d %d", &n, inicio, termino);
  // se verifica que las entradas sean correctas, esto es que el computador de
  // entrada sea distinto al de salida
  if (*inicio == *termino) {
    n = -1;
    return;
  }

  // se aplica un offset puesto que los indices de los computadores comienzan en
  // 1 y no en 0
  *inicio = *inicio - 1;
  *termino = *termino - 1;

  *grafo = malloc(n * n * sizeof(int));
  char *c = NULL;
  int l = 0;
  char *ptr;
  int k = 0;
  // lectura de matriz (grafo)
  while (fgets(linea, 1000, entrada) != NULL) {
    // terminar linea
    c = strchr(linea, '\n');
    if (c) *c = 0;
    c = NULL;
    c = strchr(linea, '\r');
    if (c) *c = 0;
    ptr = linea;
    while (*ptr != '\0') {
      *(*grafo + k) = parseStringInt(&ptr);
      k++;
    }
    l++;
  }
  // se cierra el archivo de entrada una vez leido
  fclose(entrada);
}

int main() {
  // se pide la cantidad de nodos(computadores) el computador de partida y el de
  // termino;
  // variables que indican el nodo inicio y el nodo destino
  int inicio;
  int termino;
  int *grafo;
  entradaArchivos(&inicio, &termino, &grafo);
  if (grafo == NULL || n == -1) {
    printf("error entrada de datos");
    return 0;
  }
  // se obtiene una copia del grafo original
  int *grafoOriginal = malloc(n * n * sizeof(int));
  int i, j;
  for (i = 0; i < n * n; i++) {
    *(grafoOriginal + i) = *(grafo + i);
  }
  // se genera un arreglo para determinar si un nodo ha sido visitado (utilizado
  // en algoritmo de busqueda)
  char *visitado = (char *)malloc(sizeof(char) * n);
  // comienzo iteracion
  int flujoTotal = 0;
  while (1) {
    // este ciclo se repetira mientras exista un camino valido desde el origen
    // hasta el nodo termino en cada etapa se incrementa el flujo total con cada
    // flujo parcial encontrado
    int flujoRecorrido = flujoMaximo(grafo, inicio, termino, visitado, 0);
    if (flujoRecorrido == 0) break;
    flujoTotal += flujoRecorrido;
  }
  // si al terminado este punto no se encuentra ningun camino valido se informa
  // por pantalla y el programa termina
  if (flujoTotal == 0) {
    printf("NO HAY CONEXION\n");
    return 0;
  }

  // una vez terminado el algoritmo se debe
  // comparar el grafo original con el resultante de este modo se pueden
  // combinar y asi obtener los recorridos finales, notar que el maximo flujo
  // del grafo ya ha sido encontrado la razon de esto es poder obtener los
  // recorridos finales puesto que en el algoritmo se pueden producir conexiones
  // con sentidos opuestos y valores distintos al grafo original. Para lograr
  // esto se sabe que una vez vez terminado el algoritmo el grafo utilizado
  // posee los valores residuales con lo cual solo es necesario calcular la
  // diferencia entre las conexiones originales con las residuales
  // de este modo el grafo resultante se podra recorrer sin existir caminos
  // invalidos
  // puesto que si un camino resulta con un valor negativo esto significa que el
  // flujo es contrario este se vuelve nulo esto se permite puesto que el
  // algoritmo de busqueda por profundidad avanza en una sola direccion por
  // llamarlo de alguna manera
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      if (*(grafoOriginal + i * n + j) != 0) {
        *(grafoOriginal + i * n + j) -= *(grafo + i * n + j);
        if (*(grafoOriginal + i * n + j) < 0) *(grafoOriginal + i * n + j) = 0;
      }
    }
  }

  while (1) {
    // repite de cierta manera el algoritmo con el grafo ya reducido para
    // obtener los caminos finales que junto con el flujo que aporta cada uno
    // en esta ocacion se imprimen los caminos al ser estos los definitivos
    int flujoRecorrido =
        flujoMaximo(grafoOriginal, inicio, termino, visitado, 1);
    if (flujoRecorrido == 0) break;
  }
  // se imprime el flujo total calculado anteriormente
  printf("flujo total: %d\n", flujoTotal);

  return 0;
}
