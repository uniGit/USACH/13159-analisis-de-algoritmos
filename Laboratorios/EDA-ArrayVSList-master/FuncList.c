#include <stdio.h>
#include <stdlib.h>

#include "TDAList.h"
#include "FuncHeader.h"


/*
ENTRADA:	char
SALIDA:		Puntero a String
FUNCION:	Crea un String y le asigna en la casilla c el char ingresado
*/
String* newString(char ch)
{
	String* s = (String*)malloc(sizeof(String));
	s->c = ch;
	s->sgte = NULL;
	return s;
}

/*
ENTRADA:	Puntero a String
SALIDA:		Entero
FUNCION:	Calcula la longitud del String
*/
int StringLength(String* s)
{
	String* aux = s;
	int i = 0;
	if (s != NULL)
	{
		i++;
		while (aux->sgte != NULL)
		{
			i++; aux = aux->sgte;
		}
	}
	return i;
}

/*
ENTRADA:	char y puntero a String
SALIDA:		puntero a String
FUNCION:	toma un string y devuelve el mismo con un caracter a�adido
*/
String* addChar(String* s, char c)
{
	//printf("%c", c);
	if (s != NULL)
	{
		String* aux = s;
		while (aux->sgte != NULL)	{ aux = aux->sgte; }
		aux->sgte = newString(c);
		return s;
	}
	else
	{
		String* nuevo = newString(c);
		return nuevo;
	}
}

/*
ENTRADA:	punetro a String
SALIDA:		-	
FUNCION:	Muestra los caracteres de un String en consola
*/
void showString(String* s)
{
	String* aux = s;
	while (aux->sgte != NULL)	{ printf("%c", aux->c); aux = aux->sgte; }
	printf("%c", aux->c);
}


Date* newDate(int day, int month, int year)
{
	Date* date = (Date*)malloc(sizeof(Date));
	date->day = day;
	date->month = month;
	date->year = year;
	return date;
}

Data* newData(int id, String* oname, String* ofname, String* omname, String* name, String* especie, String* numero, int edad, int atenciones, int vacunaAldia, Date* date)
{
	Data* data = (Data*)malloc(sizeof(Data));
	data->id = id;
	data->ownerName = oname;
	data->ownerFName = ofname;
	data->ownerMName = omname;
	data->name = name; 
	data->especie = especie;
	data->numero = numero;
	data->edad = edad;
	data->atenciones = atenciones;
	data->vacunaAlDia = vacunaAldia;
	data->proxControl = date;
	data->run = 0;
	return data;
}

Registro* newRegistro(Data* data)
{
	Registro* reg = (Registro*)malloc(sizeof(Registro));
	reg->data = data;
	reg->sgte = NULL;
	return reg;
}

Registro* addRegistro(Registro* reg, Data* data)
{
	if (reg != NULL)
	{
		Registro* aux = reg;
		while (aux->sgte != NULL)	{ aux = aux->sgte; }
		aux->sgte = newRegistro(data);
		return reg;
	}
	else
	{
		Registro* nuevo = newRegistro(data);
		return nuevo;
	}

	Registro* aux = reg;
	while (aux->sgte != NULL)	{ aux = aux->sgte; }
	aux = aux->sgte;
	aux->data = data;
	aux->sgte = NULL;
	return reg;
}

void showData(Data* data)
{
	showString(data->ownerName);
	printf(" ");
	showString(data->ownerFName);
	printf(" ");
	showString(data->ownerMName);
	printf(" ");
	showString(data->name);
	printf(" ");
	showString(data->especie);
	printf(" %d ", data->edad);
	showString(data->numero);
	printf(" %d ", data->atenciones);
	if( data->vacunaAlDia ) { printf("%s ", "si"); }
	else { printf("%s ", "no"); }
	printf("%d/%d/%d\n", data->proxControl->day, data->proxControl->month, data->proxControl->year);
	if(data->run) { printf("%d", data->run); }
}

void showRegistros(Registro* reg)
{
	Registro* aux = reg;
	if(reg == NULL) 	{ return; }
	while(aux != NULL)
	{
		printf("%d- ",aux->data->id);
		showData(aux->data);
		aux = aux->sgte;
	}
	printf("\n");
}