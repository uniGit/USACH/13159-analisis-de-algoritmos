#include <stdio.h>
#include <stdlib.h>

#include "FuncHeader.h"
#include "TDAList.h"


int main()
{
	Registro* reg = readFile();

	int option = 0;
	while(option < 4)
	{
		showRegistros(reg);
		printf("Ingrese una opcion:\n");
		printf("1- Modificar la informacion de una mascota\n");
		printf("2- Agregar un registro\n");
		printf("3- Eliminar un registro\n");
		printf("4- Salir.\n");
		scanf("%d", &option);
		
		if(option == 1)	{ modificarOpcion(reg); }
		if(option == 2)	{ reg = agregarRegistro(reg); }
		if(option == 3) 
		{ 
			int id;
			printf("Ingrese la id: ");
			scanf("%i", &id);
			reg = eliminarRegistro(reg, id);
		}
		if(option == 4)	{ break; }
		showRegistros(reg);
	}
	
}
