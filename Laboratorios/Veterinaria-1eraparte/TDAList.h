#ifndef TDA_H_
#define TDA_H_


typedef struct ch
{
	char c;
	struct ch* sgte;
}String;

typedef struct fecha
{
    int day;
    int month;
    int year;
}Date;

typedef struct datos
{
    int id;
    int run;
    String* ownerName;
    String* ownerFName;
    String* ownerMName;
    String* name;
    String* especie;
    int edad;
    String* numero;
    int atenciones;
    int vacunaAlDia;
    Date* proxControl;
}Data;

typedef struct reg
{
	Data* data;
	struct reg* sgte;
}Registro;


String* newString(char c);
int StringLength(String* s);
String* addChar(String* s, char c);
void showString(String* s);


Date* newDate(int dat, int month, int year);

Data* newData(int id, String* oname, String* ofname, String* omname, String* name, String* especie, String* numero, int edad, int atenciones, int vacunaAldia, Date* date);

Registro* newRegistro(Data* data);
Registro* addRegistro(Registro* reg, Data* data);
void showRegistros(Registro* reg);

#endif