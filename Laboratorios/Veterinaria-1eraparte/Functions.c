#include <stdio.h>
#include <stdlib.h>

#include "FuncHeader.h"
#include "TDAList.h"


/*
ENTRADA:	-
SALIDA:		Entero: 1 si termino todo bien. 0 si no se pudo abrir algun archivo
ALGORITMO:	Para cada linea del archivo Bultos.in: 
				Guardar Nombre del dueno
				Apellido
				etc
*/
Registro* readFile()
{
	FILE* bultos = fopen("Bultos.in", "r");	//Abre el archivo bultos.in en modo lectura

	char c = ' ';
	int edad;
	int atenciones;
	int vacunasAlDia;
	int id = 1;
	int dia;
	int mes;
	int ano;
	Data* dataFILL = newData(0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL);
	Registro* reg = NULL;

	if(!bultos) { return NULL; }
	while(1)
	{
		if(feof(bultos)) { return reg; }
		Data* data;
		// Nombre dueno
		fscanf(bultos, "%c", &c);
		String* oname = newString(c);
		while(c != ' ')
		{
			fscanf(bultos, "%c", &c);
			if(c == ' ') { break; }
			oname = addChar(oname, c);
		}
		//showString(oname);

		//Apellido paterno
		fscanf(bultos, "%c", &c);
		String* ofname = newString(c);
		while(c != ' ')
		{
			fscanf(bultos, "%c", &c);
			if(c == ' ') { break; }
			ofname = addChar(ofname, c);
		}
		//showString(ofname);

		//Apellido materno
		fscanf(bultos, "%c", &c);
		String* omname = newString(c);
		while(c != ' ')
		{
			fscanf(bultos, "%c", &c);
			if(c == ' ') { break; }
			omname = addChar(omname, c);
		}
		//showString(omname);

		//Apellido nombre mascota
		fscanf(bultos, "%c", &c);
		String* name = newString(c);
		while(c != ' ')
		{
			fscanf(bultos, "%c", &c);
			if(c == ' ') { break; }
			name = addChar(name, c);
		}
		//showString(name);

		//Especie
		fscanf(bultos, "%c", &c);
		String* especie = newString(c);
		while(c != ' ')
		{
			fscanf(bultos, "%c", &c);
			if(c == ' ') { break; }
			especie = addChar(especie, c);
		}
		//showString(especie);

		//Edad
		fscanf(bultos, "%i", &edad);

		//telefono
		fscanf(bultos, " %c", &c);
		String* telefono = newString(c);
		while(c != ' ')
		{
			fscanf(bultos, "%c", &c);
			if(c == ' ') { break; }
			telefono = addChar(telefono, c);
		}
		//showString(telefono);
		
		//Atenciones
		fscanf(bultos, "%i ", &atenciones);
		//printf("%d", atenciones);

		//Vacunas al dia
		fscanf(bultos, "%c", &c);
		if(c == 's')	{ vacunasAlDia = 1; }
		else { vacunasAlDia = 0; }
		fscanf(bultos, "%c", &c);
		fscanf(bultos, "%c", &c);

		//fecha prox control
		fscanf(bultos, "%i/%i/%i\n", &dia, &mes, &ano);

		//Se crea la estructura Date
		Date* date = newDate(dia, mes, ano);
		//printf("%d, %d, %d,", date->day, date->month, date->year);
		//Con todos los datos, se crea la estructura data
		data = newData(id, oname, ofname, omname, name, especie, telefono, edad, atenciones, vacunasAlDia, date);
		id++;
		reg = addRegistro(reg, data);
	}
	return reg;
}

Registro* agregarRegistro(Registro* reg)
{
	printf("Ingrese el nombre del dueno en un caracter por linea, si termino ingrese un punto (.):\n");
	String* nombre = newString(' ');

	char c = ' ';
	while(c != '.')
	{
		scanf(" %c", &c);
		if(c == '.' || c == '\n') { break; }
		nombre = addChar(nombre, c);
	}
	nombre = nombre->sgte;

	printf("Ingrese el apellido paterno en un caracter por linea, si termino ingrese un punto (.):\n");
	String* apellido = newString(' ');

	c = ' ';
	while(c != '.')
	{
		scanf(" %c", &c);
		if(c == '.' || c == '\n') { break; }
		apellido = addChar(apellido, c);
	}
	apellido = apellido->sgte;

	printf("Ingrese el apellido materno en un caracter por linea, si termino ingrese un punto (.):\n");
	String* apellido2 = newString(' ');

	c = ' ';
	while(c != '.')
	{
		scanf(" %c", &c);
		if(c == '.' || c == '\n') { break; }
		apellido2 = addChar(apellido2, c);
	}
	apellido2 = apellido2->sgte;

	printf("Ingrese el nombre en un caracter por linea, si termino ingrese un punto (.):\n");
	String* name = newString(' ');

	c = ' ';
	while(c != '.')
	{
		scanf(" %c", &c);
		if(c == '.' || c == '\n') { break; }
		name = addChar(name, c);
	}
	name = name->sgte;

	printf("Ingrese la especie en un caracter por linea, si termino ingrese un punto (.):\n");
	String* especie = newString(' ');

	c = ' ';
	while(c != '.')
	{
		scanf(" %c", &c);
		if(c == '.' || c == '\n') { break; }
		especie = addChar(especie, c);
	}
	especie = especie->sgte;

	printf("Ingrese la edad de la mascota:\n");
	int edad;
	scanf("%i", &edad);

	printf("Ingrese el numero de telefono en un caracter por linea, si termino ingrese un punto (.):\n");
	String* numero = newString(' ');

	c = ' ';
	while(c != '.')
	{
		scanf(" %c", &c);
		if(c == '.' || c == '\n') { break; }
		numero = addChar(numero, c);
	}
	numero = numero->sgte;

	printf("Ingrese el numero de atenciones:\n");
	int atenciones;
	scanf(" %i", &atenciones);

	printf("Ingrese 1 si tienes las vacunas al dia o 0 si no:\n");
	int vacunasAlDia;
	scanf("%d", &vacunasAlDia);

	printf("Ingrese el dia del proximo control:\n");
	int dia;
	int mes;
	int ano;
	scanf("%i", &dia);
	printf("Ingrese el mes del proximo control:\n");
	scanf("%i", &mes);
	printf("Ingrese el ano del proximo control:\n");
	scanf("%i", &ano);
	
	Date* date = newDate(dia, mes, ano);
	Data* data = newData(0, nombre, apellido, apellido2, name, especie, numero, edad, atenciones, vacunasAlDia, date);
	reg = addRegistro(reg, data);
	return reg;
}

void modificarOpcion(Registro* reg)
{
	int id;
	int opcion;
	printf("Ingrese el ID de la mascota: ");
	scanf("%i", &id);
	printf("Que desea modificar:\n");
	printf("1- Nombre del dueno\n");
	printf("2- Apellido paterno\n");
	printf("3- Apellido materno\n");
	printf("4- Nombre de la mascota\n");
	printf("5- Edad\n");
	printf("6- Numero de telefono\n");
	printf("7- Numero de atenciones\n");
	printf("8- Vacunas\n");
	printf("9- Fecha del proximo control\n");
	printf("10- Agregar RUN\n");
	scanf("%i", &opcion);

	if(opcion == 1) 
	{
		printf("Ingrese el nombre en un caracter por linea, si termino ingrese un punto (.):\n");
		String* nombre = newString(' ');

		char c = ' ';
		while(c != '.')
		{
			scanf(" %c", &c);
			if(c == '.' || c == '\n') { break; }
			nombre = addChar(nombre, c);
		}
		
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->ownerName = nombre->sgte; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 2) 
	{
		printf("Ingrese el apellido paterno en un caracter por linea, si termino ingrese un punto (.):\n");
		String* nombre = newString(' ');

		char c = ' ';
		while(c != '.')
		{
			scanf(" %c", &c);
			if(c == '.' || c == '\n') { break; }
			nombre = addChar(nombre, c);
		}
		
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->ownerFName = nombre->sgte; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 3) 
	{
		printf("Ingrese el apellido materno en un caracter por linea, si termino ingrese un punto (.):\n");
		String* nombre = newString(' ');

		char c = ' ';
		while(c != '.')
		{
			scanf(" %c", &c);
			if(c == '.' || c == '\n') { break; }
			nombre = addChar(nombre, c);
		}
		
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->ownerMName = nombre->sgte; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 4) 
	{
		printf("Ingrese el nombre en un caracter por linea, si termino ingrese un punto (.):\n");
		String* nombre = newString(' ');

		char c = ' ';
		while(c != '.')
		{
			scanf(" %c", &c);
			if(c == '.' || c == '\n') { break; }
			nombre = addChar(nombre, c);
		}
		
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->name = nombre->sgte; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 5) 
	{
		printf("Ingrese la edad:\n");
		int edad;
		scanf(" %i", &edad);
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->edad = edad; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 6) 
	{
		printf("Ingrese el numero en un caracter por linea, si termino ingrese un punto (.):\n");
		String* nombre = newString(' ');

		char c = ' ';
		while(c != '.')
		{
			scanf(" %c", &c);
			if(c == '.' || c == '\n') { break; }
			nombre = addChar(nombre, c);
		}
		
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->numero = nombre; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 7) 
	{
		printf("Ingrese el numero de atenciones:\n");
		int numero;
		scanf(" %i", &numero);
		
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->atenciones = numero; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 8) 
	{
		printf("Ingrese 1 si tiene las vacunas al dia y 0 si no\n");
		int vacuna;
		scanf(" %i", &vacuna);
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->vacunaAlDia = vacuna; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 9) 
	{
		int dia;
		int mes;
		int year;
		printf("Ingrese el dia:\n");
		scanf(" %i", &dia);
		printf("Ingrese el mes:\n");
		scanf(" %i", &mes);
		printf("Ingrese el ano:\n");
		scanf(" %i", &year);

		Date* date = newDate(dia, mes, year);
		
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->proxControl = date; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	if(opcion == 10)
	{
		printf("Ingrese el run:\n");
		int run;
		scanf(" %i", &run);
		Registro* aux = reg;
		while(aux != NULL)
		{
			if(aux->data->id == id)	{ aux->data->run = run; break; }
			aux = aux->sgte;
		}
		printf("ID %d cambiado correctamente\n", id);
		printf("\n");
		showRegistros(reg);
	}
	return;
}

Registro* eliminarRegistro(Registro* reg, int id)
{
	Registro* aux = reg;
	if(aux->data->id == id)
	{
		return reg->sgte;
	}
	while(aux->sgte != NULL)
	{
		if( aux->sgte->data->id == id)
		{
			aux->sgte = aux->sgte->sgte;
			break;
		}
		aux = aux->sgte;
	}
	return reg;
}