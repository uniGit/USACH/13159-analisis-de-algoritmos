#ifndef FUNCHEADER_H_
#define FUNCHEADER_H_

#include "TDAList.h"


Registro* readFile();
void modificarOpcion(Registro* reg);
Registro* agregarRegistro(Registro* reg);
Registro* eliminarRegistro(Registro* reg, int id);


#endif