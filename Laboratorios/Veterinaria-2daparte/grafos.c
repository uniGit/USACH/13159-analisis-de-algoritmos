#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Lista de nodos adyacentes: Almacena índice y distancia (lista lineal simple)
struct adyacentes {
  unsigned index;
  unsigned distancia;
  struct adyacentes* next;
};

struct camino {
  unsigned index;
  struct camino* next;
};

// Nodo o vértice
struct Nodo {
  char conocido;  // Bool
  char consultorio[30];
  char especialidad[30];
  unsigned max_pacientes;
  unsigned pacientes;
  unsigned distanciaLlegada;
  struct adyacentes* adyacentes;  // lista adyacente
  struct camino* link;
};

// Grafo se representa como un arreglo de nodos
// luego los nodos se relacionan unos con otros formando el grafo
struct Grafo {
  unsigned index;
  unsigned V;
  struct Nodo* array;
};

int strToInt(char texto[]) {
  int i = 0;
  int buff = 0;
  while (texto[i] >= '0' && texto[i] <= '9') {
    buff *= 10;
    buff += texto[i++] - '0';
  }
  return buff;
}

char esNum(char c) {
  if (c >= '0' && c <= '9') return 1;
  return 0;
}

struct Nodo textoAnodo(char lineaTexto[100]) {
  struct Nodo nuevoConsultorio;
  int pos = -1;
  int i = 0;
  int numero = 0;
  // leer nombre
  while (lineaTexto[++pos] != ' ') {
    nuevoConsultorio.consultorio[i++] = lineaTexto[pos];
  }
  nuevoConsultorio.consultorio[i] = '\0';
  i = 0;
  // leer especialidad
  while (lineaTexto[++pos] != ' ') {
    nuevoConsultorio.especialidad[i++] = lineaTexto[pos];
  }
  nuevoConsultorio.especialidad[i] = '\0';

  // leer maximo pacientes
  while (esNum(lineaTexto[++pos])) {
    numero *= 10;
    numero += (lineaTexto[pos] - '0');
  }
  nuevoConsultorio.max_pacientes = numero;
  numero = 0;

  // leer numero pacientes
  while (esNum(lineaTexto[++pos])) {
    numero *= 10;
    numero += (lineaTexto[pos] - '0');
  }
  nuevoConsultorio.pacientes = numero;

  // se retorna el struct consultorio
  nuevoConsultorio.distanciaLlegada = INT_MAX;
  nuevoConsultorio.adyacentes = NULL;
  nuevoConsultorio.link = NULL;
  return nuevoConsultorio;
};

struct Grafo* crearGrafo() {
  char lineaTexto[100];
  FILE* consultorios = fopen("Consultorios.in", "r");
  if (consultorios == NULL) {
    fputs("Archivo consultorios no encontrado\n", stderr);
    exit(1);
  }
  // se lee primera linea para tener el numero de consultorios
  fgets(lineaTexto, 100, consultorios);
  int V = strToInt(lineaTexto);

  // crear grafo e inicializar (v vertices)
  struct Grafo* Grafo = (struct Grafo*)malloc(sizeof(struct Grafo));
  Grafo->index = 0;
  Grafo->V = V;
  // se crea el arreglo de v nodos
  Grafo->array = (struct Nodo*)malloc(V * sizeof(struct Nodo));

  int i;
  struct Nodo aux;
  for (i = 0; i < V; i++) {
    fgets(lineaTexto, 100, consultorios);
    aux = textoAnodo(lineaTexto);
    Grafo->array[i] = aux;
    // copiar arreglos dentro de structs es valido (memcpy())
  }

  fclose(consultorios);
  return Grafo;
}

// Adds an edge to an undirected Grafo
void addEdge(struct Grafo* grafo, int inicio, int destino, unsigned distancia) {
  // se crean los caminos del grafo
  // se crea relacion con el nodo destino y distancia en forma de un elemento de
  // la lista de enlace
  struct adyacentes* aux = grafo->array[inicio].adyacentes;
  grafo->array[inicio].adyacentes = malloc(sizeof(struct adyacentes));
  grafo->array[inicio].adyacentes->index = destino;
  grafo->array[inicio].adyacentes->distancia = distancia;
  grafo->array[inicio].adyacentes->next = aux;

  // es un grafo no direccionado por lo que es reflexivo
  aux = grafo->array[destino].adyacentes;
  grafo->array[destino].adyacentes = malloc(sizeof(struct adyacentes));
  grafo->array[destino].adyacentes->index = inicio;
  grafo->array[destino].adyacentes->distancia = distancia;
  grafo->array[destino].adyacentes->next = aux;
}

void buscarIndices(struct Grafo* grafo, char lineaTexto[100], int* inicio,
                   int* destino, int* distancia) {
  int V = grafo->V;
  char consultorio1[30];
  char consultorio2[30];
  int pos = -1;
  int i = 0;

  // leer nombre1
  while (lineaTexto[++pos] != ' ') {
    consultorio1[i++] = lineaTexto[pos];
  }
  consultorio1[i] = '\0';
  i = 0;
  // leer nombre2
  while (lineaTexto[++pos] != ' ') {
    consultorio2[i++] = lineaTexto[pos];
  }
  consultorio2[i] = '\0';

  *distancia = strToInt(&lineaTexto[++pos]);

  for (i = 0; i < V; i++) {
    i = 0;
    for (i = 0; i < V; i++) {
      if (strcmp(grafo->array[i].consultorio, consultorio1) == 0) {
        *inicio = i;
      }
      if (strcmp(grafo->array[i].consultorio, consultorio2) == 0) {
        *destino = i;
      }
    }
    // se tienen los nombres a buscar de ambos consultorios y la distancia entre
    // ellos
  }
}

void conectargrafo(struct Grafo* grafo) {
  // Se lee el archivo que contiene los caminos del grafo y los tiempos
  int i;
  char lineaTexto[100];
  FILE* consultorios = fopen("DondeLlevarAlBulto.in", "r");
  if (consultorios == NULL) {
    fputs("Archivo tiempos de viaje no encontrado\n", stderr);
    exit(1);
  }
  // se lee primera linea para tener el numero de consultorios
  fgets(lineaTexto, 100, consultorios);
  int V = strToInt(lineaTexto);
  int inicio, destino, distancia;
  for (i = 0; i < V; i++) {
    fgets(lineaTexto, 100, consultorios);
    buscarIndices(grafo, lineaTexto, &inicio, &destino, &distancia);
    addEdge(grafo, inicio, destino, distancia);
  }

  fclose(consultorios);
}

char existeDesconocido(struct Grafo* grafo) {
  int i;
  for (i = 0; i < grafo->V; i++) {
    if (grafo->array[i].distanciaLlegada == INT_MAX) return 1;
  }
  return 0;
}

int menorDesconocido(struct Grafo* grafo) {
  int i;
  int minDist = INT_MAX;
  int indice = -1;
  for (i = 0; i < grafo->V; i++) {
    if (grafo->array[i].conocido == 0 &&
        grafo->array[i].distanciaLlegada < minDist) {
      minDist = grafo->array[i].distanciaLlegada;
      indice = i;
    }
  }
  return indice;
}

struct camino* cpyList(struct camino* lista) {
  if (!lista) return NULL;
  struct camino* nuevaLista = malloc(sizeof(struct camino));
  nuevaLista->index = lista->index;
  nuevaLista->next = cpyList(lista->next);
  return nuevaLista;
}

// resetear nodos del grafo (conexiones y lista de recorrido)
void liberarAdy(struct adyacentes* lista) {
  struct adyacentes* aux;
  while (lista) {
    aux = lista->next;
    free(lista);
    lista = aux;
  }
}

void liberarLink(struct camino* lista) {
  struct camino* aux;
  while (lista) {
    aux = lista->next;
    free(lista);
    lista = aux;
  }
}

void reset(struct Grafo* grafo) {
  int i;
  for (i = 0; i < grafo->V; i++) {
    liberarLink(grafo->array[i].link);
    grafo->array[i].link = NULL;
  }
}

void deleteGrafo(struct Grafo* grafo) {
  if (grafo) {
    reset(grafo);
  }
  int i;
  for (i = 0; i < grafo->V; i++) {
    liberarLink(grafo->array[i].link);
  }
  free(grafo->array);
}

void dijkstra(struct Grafo* grafo, int inicio) {
  int i;
  int indiceMin;
  int costo;
  struct adyacentes* adyacentes;
  // inicializar el grafo con distancias infinitas y nodos no recorridos
  // (desconocidos)
  for (i = 0; i < grafo->V; i++) {
    grafo->array[i].distanciaLlegada = INT_MAX;
    grafo->array[i].conocido = 0;
  }
  // se selecciona el nodo de partida
  grafo->array[inicio].distanciaLlegada = 0;
  while (existeDesconocido(grafo)) {
    indiceMin = menorDesconocido(grafo);
    grafo->array[indiceMin].conocido = 1;
    // se recorren todos los adyacentes del nodo
    adyacentes = grafo->array[indiceMin].adyacentes;
    while (adyacentes) {
      // si el nodo adyacente no es conocido
      if (grafo->array[adyacentes->index].conocido == 0) {
        costo = adyacentes->distancia;
        if (grafo->array[indiceMin].distanciaLlegada + costo <
            grafo->array[adyacentes->index].distanciaLlegada) {
          // se actualiza adyacente y se anexa el indice al recorrido
          grafo->array[adyacentes->index].distanciaLlegada =
              grafo->array[indiceMin].distanciaLlegada + costo;

          struct camino* aux = cpyList(grafo->array[indiceMin].link);
          if (grafo->array[adyacentes->index].link)
            //grafo bidireccional
            liberarLink(grafo->array[adyacentes->index].link);
          grafo->array[adyacentes->index].link = malloc(sizeof(struct camino));
          grafo->array[adyacentes->index].link->index = indiceMin;
          grafo->array[adyacentes->index].link->next = aux;
        }
      }
      adyacentes = adyacentes->next;
    }
  }
}

void imprimirRecorrido(FILE* out, struct Grafo* grafo,
                       struct camino* recorrido) {
  if (recorrido) {
    imprimirRecorrido(out, grafo, recorrido->next);
    fprintf(out, "-> %s", grafo->array[recorrido->index].consultorio);
  }
}

int buscarIndiceNombre(struct Grafo* grafo, char nombre[30]) {
  int i;
  for (i = 0; i < grafo->V; i++) {
    if (strcmp(grafo->array[i].consultorio, nombre) == 0) return i;
  }
  return -1;
}

int buscarIndiceEspecialidad(struct Grafo* grafo, char especialidad[30]) {
  int i;
  for (i = 0; i < grafo->V; i++) {
    if (strcmp(grafo->array[i].especialidad, especialidad) == 0) return i;
  }
  return -1;
}

int buscarValido(struct Grafo* grafo, char especialidad[30]) {
  int i;
  int indice = -1;
  int minDis = INT_MAX;
  // se busca el consultorio mas cercano que tenga vacantes disponibles
  for (i = 0; i < grafo->V; i++) {
    if (grafo->array[i].distanciaLlegada < minDis &&
        grafo->array[i].max_pacientes > grafo->array[i].pacientes &&
        strcmp(grafo->array[i].especialidad, especialidad) == 0) {
      minDis = grafo->array[i].distanciaLlegada;
      indice = i;
    }
  }
  return indice;
}

void ingresarPaciente(struct Grafo* Grafo) {
  FILE* out = fopen("WiiuuWiiuu.out", "a");
  char nombre[30];
  char especialidad[30];
  int i;
  int indice;
  int indice2;
  int tiempoTotal = 0;
  while (1) {
    do {
      printf("Ingresar nombre partida, dejar vacio para salir\n");
      fgets(nombre, 30, stdin);
      if (nombre[0] == '\n') return;
      i = -1;
      while (nombre[++i] != '\n')
        ;
      nombre[i] = '\0';
      indice = buscarIndiceNombre(Grafo, nombre);
      if (indice == -1) printf("Nombre de consultorio no encontrado\n");
    } while (indice == -1);

    do {
      printf("Ingresar especialidad\n");
      fgets(especialidad, 30, stdin);
      i = -1;
      while (especialidad[++i] != '\n')
        ;
      especialidad[i] = '\0';
      indice2 = buscarIndiceEspecialidad(Grafo, especialidad);
      if (indice2 == -1) printf("Nombre de especialidad no encontrado\n");
    } while (indice2 == -1);

    dijkstra(Grafo, indice);
    int indiceValido = buscarValido(Grafo, especialidad);
    if (indiceValido == -1) {
      printf("No hay consultorios disponibles con esa especialidad\n");
    } else {
      printf("Generando archivo...\n");

      fprintf(out, "\n");
      tiempoTotal += Grafo->array[indiceValido].distanciaLlegada;

      imprimirRecorrido(out, Grafo, Grafo->array[indiceValido].link);
      fprintf(out, "-> %s", Grafo->array[indiceValido].consultorio);
      fprintf(out, " Vacantes: %d ",
              Grafo->array[indiceValido].max_pacientes -
                  Grafo->array[indiceValido].pacientes);
      Grafo->array[indiceValido].pacientes++;
      fprintf(out, " Tiempo: %d ", Grafo->array[indiceValido].distanciaLlegada);
      fprintf(out, " Tiempo total acumulado: %d ", tiempoTotal);
    }
    reset(Grafo);
  }
  fclose(out);
}

int main() {
  FILE* out = fopen("WiiuuWiiuu.out", "w");
  fclose(out);
  struct Grafo* Grafo = crearGrafo();  // consultorios.in
  conectargrafo(Grafo);                // caminos y distancias
  ingresarPaciente(Grafo);
  deleteGrafo(Grafo);
  free(Grafo);
  return 0;
}
