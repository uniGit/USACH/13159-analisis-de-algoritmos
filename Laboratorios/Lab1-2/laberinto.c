#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// variables globales dimensiones laberinto
int x, y;

char movValido(int i, int j, char *lab) {
  if (*(lab + y * i + j) == '.') return 1;
  if (*(lab + y * i + j) == 'E') return 1;
  if (*(lab + y * i + j) == 'K') return 1;
  if (*(lab + y * i + j) == 'S') return 1;
  return 0;
}

void salida(char *lab, int i, int j, char b, char *caminoSalida, int nivel,
            int *min, char *minSal) {
  // si se lleva al maximo nivel (n*m) ya no se encontro un recorrido
  // valido
  if (nivel == x * y) return;
  // se modifican los indices para que sean validos en caso de
  // salir de los rangos
  i = (i + x) % x;
  j = (j + y) % y;
  // en caso de que la posicion i,j sea una posicion valida
  if (movValido(i, j, lab)) {
    if (*(lab + y * i + j) == b) {  // se encuentra llave o salida
      // se agrega el elemento al arreglo que
      // contiene el camino recorrido (solucion parcial)
      *(caminoSalida + y * i + j) = b;
      // se marca el arreglo laberinto con el fin de no pasar
      // por la misma posicion otra vez (marcar para evitar recorrer en
      // circulos)
      *(lab + y * i + j) += 'A';
      // de esta manera de sejan rastros para evitar moverce en circulos
      if (nivel < *min) {
        // si el nivel actual es menor al minimo global entonces se toma esta
        // solucion como solucion candidata
        for (int i = 0; i < x * y; i++) *(minSal + i) = *(caminoSalida + i);
        // esta solucion candidata se vuelve el nuevo minimo global
        *min = nivel;
      }
    } else {  // en el caso de que no se encuentre la llave en la posicion i,j
              // se sigue buscando
      // se agrega el punti i,j a la solucion parcial
      if (*(lab + y * i + j) == '.') *(caminoSalida + y * i + j) = 'X';
      // se marca el punto i,j para no volver pa pasar por el
      *(lab + y * i + j) += 'A';
      // en este punto se procede a buscar una solucion con los cuatro
      // movimientos posibles que permite el punto i,jk se incrementa en 1 el
      // nivel
      salida(lab, i, j + 1, b, caminoSalida, nivel + 1, min, minSal);
      salida(lab, i, j - 1, b, caminoSalida, nivel + 1, min, minSal);
      salida(lab, i + 1, j, b, caminoSalida, nivel + 1, min, minSal);
      salida(lab, i - 1, j, b, caminoSalida, nivel + 1, min, minSal);
    }
    // luego de analizar todos los recorridos posibles del punto i,j este se
    // desmarca
    *(lab + y * i + j) -= 'A';
    // sucede lo mismo con la solucion parcial
    *(caminoSalida + y * i + j) = *(lab + y * i + j);
  }
}

void strToInt(char *s, int *n, int *m) {
  // funcion que lee una linea de texto de dos numeros y entrega dichos numeros
  int i = 0;
  int acc = 0;
  while (s[i] != '\0') {
    if (s[i] == ' ') {
      *n = acc;
      acc = 0;
      i++;
      continue;
    }
    acc = acc * 10 + s[i++] - '0';
  }
  *m = acc;
}

int main() {
  // lectura de archivo
  FILE *entrada;
  entrada = fopen("Entrada.in", "r");
  char linea[1000];
  if (entrada == NULL) {
    printf("archivo no encontrado\n");
    return 0;
  }
  // lectura de dimensiones del laberinto
  char *c = NULL;
  fgets(linea, 1000, entrada);
  c = strchr(linea, '\n');
  if (c) *c = 0;
  strToInt(linea, &x, &y);
  char *laberinto = malloc(x * y * sizeof(char));
  int l = 0;
  while (fgets(linea, 1000, entrada) != NULL) {
    c = strchr(linea, '\n');
    if (c) *c = 0;
    for (int j = 0; j < y; j++) {
      *(laberinto + y * l + j) = linea[j];
    }
    l++;
  }
  fclose(entrada);
  // arreglos de soluciones parciales y soluciones optimas
  char caminoLlave[x][y];
  char caminoSalida[x][y];
  char minCam[x][y];
  char minSal[x][y];
  for (int i = 0; i < x * y; i++) {
    // se llenan los arreglos en caso de que no se encuentre solucion se
    // presenta ordenado (sin datos basura)
    caminoLlave[i / y][i % y] = *(laberinto + i);
    caminoSalida[i / y][i % y] = *(laberinto + i);
    minCam[i / y][i % y] = '*';
    minSal[i / y][i % y] = '*';
  }
  int ie, je;
  int ik, jk;
  // se busca la entrada en el laberinto y la llave
  for (int i = 0; i < x; i++) {
    for (int j = 0; j < y; j++) {
      if (*(laberinto + y * i + j) == 'E') {
        ie = i;
        je = j;
      }
      if (*(laberinto + y * i + j) == 'K') {
        ik = i;
        jk = j;
      }
    }
  }

  FILE *resultado;
  resultado = fopen("Salida.out", "w");
  if (resultado == NULL) {
    printf("archivo no se pudo crear\n");
    return 0;
  }

  // se calcula y escribe el menor camino de la entrada a la salida
  // el menor camino se deja en un numero mayor al maximo posible
  int min = x * y + 1;
  // se realiza la busqueda
  salida((char *)laberinto, ie, je, 'K', (char *)caminoLlave, 0, &min,
         (char *)minCam);
  // se busca en el laberinto la posicion de la llave para partir desde ahi al
  // momento de encontrar la salida
  for (int i = 0; i < x; i++) {
    for (int j = 0; j < y; j++) {
      fprintf(resultado, "%c", minCam[i][j]);
    }
    fprintf(resultado, "\n");
  }
  fprintf(resultado, "\n");
  // luego de encontrar el camino el algoritmo se repite para encontrar la
  // salida
  min = x * y + 1;

  salida((char *)laberinto, ik, jk, 'S', (char *)caminoSalida, 0, &min,
         (char *)minSal);
  for (int i = 0; i < x; i++) {
    for (int j = 0; j < y; j++) {
      fprintf(resultado, "%c", minSal[i][j]);
    }
    fprintf(resultado, "\n");
  }
  fclose(resultado);
  return 0;
}