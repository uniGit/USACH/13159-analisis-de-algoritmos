#include "indice_agenda.h"

/**
  Crea un nuevo indice de agenda a partir asociado a un registro
  Parametros
    Registro *registro: Puntero al registro que conforma el indice
  Retorna
    IndiceAgenda*: Puntero al indice creado
*/
IndiceAgenda *crearIndiceAgenda(Registro *registro){
  IndiceAgenda *nuevo_indice_agenda = (IndiceAgenda*)malloc(sizeof(IndiceAgenda));
  nuevo_indice_agenda->izquierda = NULL;
  nuevo_indice_agenda->derecha = NULL;
  nuevo_indice_agenda->registro = registro;
  return nuevo_indice_agenda;
}
/**
  Indica la direccion donde debe encontrarse el registro dado, comparando la fecha de consulta
  Parametros
    IndiceAgenda *indice_agenda: Puntero al indice de la agenda
    Registro *registro: Registro a comparar
  Retorna
    int: -1 si se debe ir a la izquierda, 1 si es a la derecha
*/
int direccionIndiceAgenda(IndiceAgenda *indice_agenda, Registro *registro){
  int comparacion = comparaFechaConsultaRegistro(indice_agenda->registro, registro, 1);
  if(comparacion <= 0){
    return 1;
  }else{
    return -1;
  }
}

/**
  Inserta un nuevo registro al indice, manteniendo orden por fecha de consulta
  Parametros
    IndiceAgenda *indice_agenda: Puntero al indice donde agregar el registro
    Registro *registro: Puntero al registro a insertar
  Retorna
    IndiceAgenda*: Puntero al indice de la agenda con el registro agregado
*/
IndiceAgenda *insertarIndiceAgenda(IndiceAgenda *indice_agenda, Registro *registro){
  if(indice_agenda == NULL){
    indice_agenda = crearIndiceAgenda(registro);
    return indice_agenda;
  }
  int direccion = direccionIndiceAgenda(indice_agenda, registro);
  if(direccion == -1){
    if(indice_agenda->izquierda == NULL){
      indice_agenda->izquierda = crearIndiceAgenda(registro);
    }else{
      indice_agenda->izquierda = insertarIndiceAgenda(indice_agenda->izquierda, registro);
    }
  }else{
    if(indice_agenda->derecha == NULL){
      indice_agenda->derecha = crearIndiceAgenda(registro);
    }else{
      indice_agenda->derecha = insertarIndiceAgenda(indice_agenda->derecha, registro);
    }
  }
  return indice_agenda;
}

/**
  Realiza un recorrido del indice de manera ordenada por fecha de consulta
  Parametros
    IndiceAgenda *indice_agenda: Puntero al indice a recorrer
    IndiceAgenda **final_recorrido: Puntero a puntero del ultimo indice de la lista,
                  Puntero a  NULL cuando se comienza
    Registro *filtro: Puntero a un registro con el que se compara mes y anio
  Retorna
    IndiceAgenda*: Puntero a la cabeza de una lista de indices de agenda ordenados
*/
IndiceAgenda *recorrerIndice(IndiceAgenda *indice_agenda, IndiceAgenda **final_recorrido, Registro *filtro){
  IndiceAgenda *recorrido = NULL;
  if(indice_agenda != NULL){

    if(filtro != NULL){
      //Si se filtra el recorrido, al moverse hacia el registro con el mes y anio
      //buscado entonces el inorden desde ese nodo corresponde al resultado
      int direccion_recorrido = comparaFechaConsultaRegistro(indice_agenda->registro, filtro, 0);
      if(direccion_recorrido != 0){
        //Si el nodo tiene fecha mayor el inorden filtrado de la izquierda es el resultado
        if(direccion_recorrido == 1){
          return recorrerIndice(indice_agenda->izquierda, final_recorrido, filtro);
        }else{
          //Si es menor, el inorden del nodo derecho es la respuesta
          return recorrerIndice(indice_agenda->derecha, final_recorrido, filtro);
        }
      }
    }
    //Si direccion recorrido es 0 o filtro es null, el inorden izquierdo
    //filtrado si aplica es la primera parte de la lista
    recorrido = recorrerIndice(indice_agenda->izquierda, final_recorrido, filtro);
    if(recorrido == NULL){
      //Si el inorden anterior es null
      //este recorrido es la cabeza de la lista
      recorrido = crearIndiceAgenda(indice_agenda->registro);
      //Se actualiza el fin del recorrido a la cabeza de la lista
      *final_recorrido = recorrido;
      //Al fin del recorrido se le agrega a su derecha el indorden de la derecha
      (*final_recorrido)->derecha = recorrerIndice(indice_agenda->derecha, final_recorrido, filtro);
      //Si el final del inorden de la derecha no esta vacio
      if((*final_recorrido)->derecha != NULL){
        //se actualiza el fin del recorrido al fin del inorden derecho
        *final_recorrido = recorrido->derecha;
      }
    }else{
      //Si el recorrido inorden izquierdo no es nulo, se agrega a la derecha
      //del final este nodo y se actualiza el puntero al nodo final de la lista
      (*final_recorrido)->derecha = crearIndiceAgenda(indice_agenda->registro);
      *final_recorrido = (*final_recorrido)->derecha;
      //A la derecha del final de la lista se le agrega el inorden, filtrado si aplica, de la derecha
      (*final_recorrido)->derecha = recorrerIndice(indice_agenda->derecha, final_recorrido, filtro);
      //Se actualiza el final de la lista si el fin del inorden derecho no es nulo
      if((*final_recorrido)->derecha != NULL){
        *final_recorrido = (*final_recorrido)->derecha;
      }
    }
  }
  return recorrido;
}

/**
  Borra un indice que coindica con el registro dado
  Parametros
    IndiceAgenda *indice_agenda: Puntero al indice raiz donde comenzar el borrado
    Registro *registro: Puntero al registro a borrar
  Retorna
    IndiceAgenda*: Puntero al indice raiz con el indice borrado
*/
IndiceAgenda *borrarIndiceAgenda(IndiceAgenda *indice_agenda, Registro* registro){
  if(indice_agenda == NULL){
    return NULL;
  }
  if(coincideRegistro(indice_agenda->registro, registro)){
    if(indice_agenda->izquierda != NULL && indice_agenda->derecha != NULL){
      IndiceAgenda *indice_izquierda = indice_agenda->izquierda;
      indice_agenda = indice_agenda->derecha;
      IndiceAgenda *indice_menor_derecha = indice_agenda;
      while(indice_menor_derecha->izquierda != NULL){
        indice_menor_derecha = indice_menor_derecha->izquierda;
      }
      indice_menor_derecha->izquierda = indice_izquierda;
    }else if(indice_agenda->izquierda != NULL){
      return indice_agenda->izquierda;
    }else{
      return indice_agenda->derecha;
    }
  }else{
    indice_agenda->izquierda = borrarIndiceAgenda(indice_agenda->izquierda, registro);
    indice_agenda->derecha = borrarIndiceAgenda(indice_agenda->derecha, registro);
  }
  return indice_agenda;
}

/**
  Modifica un registro del indice
  Parametros
    IndiceAgenda *indice_agenda: Puntero al indice raiz donde comenzar el borrado
    Registro *registro: Puntero al registro a modificar
  Retorna
    IndiceAgenda*: Puntero al indice raiz con el indice modificado
*/
IndiceAgenda *modificarIndiceAgenda(IndiceAgenda *indice_agenda, Registro *registro){
  indice_agenda = borrarIndiceAgenda(indice_agenda, registro);
  indice_agenda = insertarIndiceAgenda(indice_agenda, registro);
  return indice_agenda;
}
