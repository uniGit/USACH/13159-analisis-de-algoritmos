#include "registro.h"

/**
  Crea un nuevo registro a partir de una linea de datos
  Parametros
    char* linea: Puntero a caracteres con la linea de datos
  Retorna
    Registro*: Puntero al registro creado
*/
Registro *crearRegistro(char* linea){
  Registro *nuevo = (Registro*)malloc(sizeof(Registro));
  char vacunas[3];
  sscanf(
    linea, "%s %s %s %s %s %d %s %d %s %s",
    nuevo->n_dueno, nuevo->ap_dueno, nuevo->am_dueno, nuevo->n_mascota,
    nuevo->especie, &(nuevo->edad), nuevo->telefono, &(nuevo->c_atenciones),
    vacunas, nuevo->sig_control
  );
  nuevo->vacunas = strcmp("si", vacunas) == 0;
  return nuevo;
}

/**
  Indica, comparando por su fecha de siguiente control, si un registro
  es mayor, igual o menor a otro
  Parametros
    Registro *r1: Puntero al registro a comparar
    Registro *r2: Puntero al registro comparando
    int incluirDia: 1 si se debe comparar la fecha completa, 0 para usar solo el mes y anio
                    como comparador
  Retorna
    int: 1, si r1 es mayor a r2, -1 si es menor, 0 si son iguales
*/
int comparaFechaConsultaRegistro(Registro *r1, Registro *r2, int incluirDia){
  //Arreglo bidimensional que contendra el dia, mes, y año de cada registro
  int d[2][3];
  sscanf(r1->sig_control, "%d/%d/%d", &(d[0][0]), &(d[0][1]), &(d[0][2]));
  sscanf(r2->sig_control, "%d/%d/%d", &(d[1][0]), &(d[1][1]), &(d[1][2]));

  //Diferencia entre dato del primer con el segundo registro
  int diff = 0;
  //Hasta que indice realizar la comprobacion (dia = indice 0)
  int fin = 0;
  //Si no se debe incluir el dia en la comprobacion, el fin es el mes (indice 1)
  if(!incluirDia) fin = 1;
  //Por cada indice de la fecha (año, mes, dia)
  for(int i = 2; i >= fin; i--){
    //Se obtiene la diferencia del indice
    diff = d[0][i] - d[1][i];
    //Si la diferencia es positiva el primero es mayor al segundo
    //Diferencia negativa implica que el primero es menor al segundo
    if(diff > 0){
      return 1;
    }else if(diff < 0){
      return -1;
    }
    //Si la diferencia es 0 se debe comprobar el siguiente indice
  }
  //Si no hubo diferencia en ningun indice, los registros son iguales en sus
  //fechas
  return 0;
}

//Compara dos registros a partir de los datos de identificacion
int coincideRegistro(Registro *r1, Registro *r2){
  return (strcmp(r1->n_dueno, r2->n_dueno) &&
    strcmp(r1->ap_dueno, r2->ap_dueno) &&
    strcmp(r1->am_dueno, r2->am_dueno) &&
    strcmp(r1->n_mascota, r2->n_mascota)) == 0;
}

/**
  Crea un registro para busqueda por fecha
*/
Registro *crearRegistroFecha(char *mes, char *anio){
  char linea[200];
  sprintf(linea, ". . . . . 0 . 0 . 01/%s/%s", mes, anio);
  return crearRegistro(linea);
}
