#include "agenda.h"

/**
  Crea una nueva agenda desde un archivo de entrada
  Parametros
    char *archivo: Ruta del archivo a cargar
  Retorna
    Agenda*: Puntero a la agenda creada
*/
Agenda *leerAgenda(char *archivo){
  FILE *archivo_agenda = fopen(archivo, "r");
  if(archivo_agenda == NULL){
    return NULL;
  }
  Agenda *nueva_agenda = crearAgenda();
  int total_entradas;
  fscanf(archivo_agenda, "%d", &(total_entradas));
  while(fgetc(archivo_agenda) != '\n');
  for (int i = 0; i < total_entradas; i++) {

    char linea[200];

    if (fgets(linea, 200, archivo_agenda) != NULL){
      Registro *registro = crearRegistro(linea);
      agregarRegistroAgenda(nueva_agenda, registro);
    }else{
      return nueva_agenda;
    }
  }
  return nueva_agenda;
}
/**
  Crea un nueva agenda vacia
  Retorna
    Agenda*: Puntero a la agenda creada
*/
Agenda *crearAgenda(){
  Agenda *nueva_agenda = (Agenda*)malloc(sizeof(Agenda));
  nueva_agenda->raiz_agenda = NULL;
  nueva_agenda->total_entradas = 0;
  return nueva_agenda;
}

/**
  Agrega un nuevo registro a la agenda
  Parametros
    Agenda *agenda: Puntero a la agenda donde agregar el registro
    Registro *registro: Puntero al registro a agregar
  Retorna
    Agenda*: Puntero a la agenda con el registro agregado
*/
Agenda *agregarRegistroAgenda(Agenda *agenda, Registro *registro){
  agenda->raiz_agenda = insertarIndiceAgenda(agenda->raiz_agenda, registro);
  agenda->total_entradas = agenda->total_entradas + 1;
  return agenda;
}

/**
  Obtiene una lista de los registros que tienen fecha de consulta para el
  mes y anio dados
  Parametros
    Agenda *agenda: Puntero a la agenda donde buscar
    char *mes: Mes a buscar en el formato MM
    char *anio: Anio a buscar, en el formato AAAA
  Retorna
    IndiceAgenda*: Puntero a la cabeza de una lista con los indices de la agenda encontrados
*/
IndiceAgenda *buscarMesAnioAgenda(Agenda *agenda, char *mes, char *anio){
  IndiceAgenda *busqueda;
  IndiceAgenda *final;
  Registro *filtro = crearRegistroFecha(mes, anio);
  return recorrerIndice(agenda->raiz_agenda, &final, filtro);
}

/**
  Elimina un registro de la agenda
  Parametros
    Agenda *agenda: Puntero a la agenda a recorrer
    Registro *registr: Puntero a registro con los datos de identificacion
  Retorna
    IndiceAgenda*: Puntero a la agenda con el registro borrado
*/
Agenda *borrarRegistroAgenda(Agenda *agenda, Registro *registro){
  agenda->raiz_agenda = borrarIndiceAgenda(agenda->raiz_agenda, registro);
  return agenda;
}

/**
  Modifica un registro de la agenda
  Parametros
    Agenda *agenda: Puntero a la agenda a modificar
    Registro *registro: Puntero al registro con los datos de identificacion y los modificados
  Retorna
    Agenda*: Puntero a la agenda modificada
*/
Agenda *modificarRegistroAgenda(Agenda *agenda, Registro *registro){
  agenda->raiz_agenda = modificarIndiceAgenda(agenda->raiz_agenda, registro);
  return agenda;
}
