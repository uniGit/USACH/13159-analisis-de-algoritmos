#include "agenda.h"
#include "stdio.h"

Registro *crearRegistroConsola(){
  Registro *r = crearRegistro("");
  printf("Datos del propietario.\n");
  printf("Nombre: ");
  scanf("%s", r->n_dueno);
  printf("Apellido paterno: ");
  scanf("%s", r->ap_dueno);
  printf("Apellido materno: ");
  scanf("%s", r->am_dueno);
  printf("Telefono: ");
  scanf("%s", r->telefono);
  printf("Datos de la mascota.\n");
  printf("Nombre: ");
  scanf("%s", r->n_mascota);
  printf("Especie: ");
  scanf("%s", r->especie);
  printf("Edad: ");
  scanf("%d", &(r->edad));
  getchar();
  r->c_atenciones = 1;
  char vacunas[] = "si";
  printf("Vacunas al dia (si/no): ");
  scanf("%s", vacunas);
  r->vacunas = (vacunas[0] == 's');
  printf("Siguiente control (DD/MM/YYYY): ");
  scanf("%s", r->sig_control);
  return r;
}

Registro *crearRegistroParaProcesar(){
  printf("Para eliminar o modificar una mascota, es necesario el nombre completo del propietario y de la mascota.\n");
  Registro *r = crearRegistro("");
  printf("Datos del propietario.\n");
  printf("Nombre: ");
  scanf("%s", r->n_dueno);
  printf("Apellido paterno: ");
  scanf("%s", r->ap_dueno);
  printf("Apellido materno: ");
  scanf("%s", r->am_dueno);
  printf("Datos de la mascota\n");
  printf("Nombre: ");
  scanf("%s", r->n_mascota);
  return r;
}

Agenda *cargarArchivoAgenda(Agenda *agenda){
  if(agenda != NULL){
    printf("Ya se cargo la informacion.\n");
    return agenda;
  }else{
    char ruta[200];
    printf("Ingrese la ruta del archivo: ");
    scanf("%s", ruta);
    agenda = leerAgenda(ruta);
    if(agenda == NULL){
      printf("No se encontro el archivo.\n");
    }
    return agenda;
  }
}

Agenda *buscarInformacionAgenda(Agenda *agenda){
  if(agenda == NULL){
    agenda = cargarArchivoAgenda(agenda);
  }
  if(agenda == NULL){
    printf("No se cargo la agenda.\n");
    return NULL;
  }else{
    char mes[3];
    char anio[5];
    printf("Ingrese mes: ");
    scanf("%s", mes);
    printf("Ingrese anio: ");
    scanf("%s", anio);
    IndiceAgenda *busqueda = buscarMesAnioAgenda(agenda, mes, anio);
    FILE *resultado_busqueda = fopen("Bultos.out", "w+");
    while(busqueda != NULL){
      Registro *r = busqueda->registro;
      char vacunas[3] = "no";
      if(r->vacunas){
        strcpy(vacunas, "si");
      }
      fprintf(resultado_busqueda,
        "%s %s %s %s %s %d %s %d %s %s\n",
        r->n_dueno, r->ap_dueno, r->am_dueno, r->n_mascota, r->especie,
        r->edad, r->telefono, r->c_atenciones, vacunas, r->sig_control
      );
      busqueda = busqueda->derecha;
    }
    fclose(resultado_busqueda);
  }
  return agenda;
}

Agenda *agregarInformacionAgenda(Agenda *agenda){
  if(agenda == NULL){
    agenda = cargarArchivoAgenda(agenda);
  }
  if(agenda == NULL){
    printf("No se cargo la agenda\n");
    return NULL;
  }else{
    Registro *r = crearRegistroConsola();
    agenda = agregarRegistroAgenda(agenda, r);
  }
  return agenda;
}

Agenda *modificarInformacionAgenda(Agenda *agenda){
  if(agenda == NULL){
    agenda = cargarArchivoAgenda(agenda);
  }
  if(agenda == NULL){
    printf("No se cargo la agenda.\n");
    return NULL;
  }else{
    Registro *r = crearRegistroParaProcesar();
    printf("DATOS A MODIFICAR.\n");
    printf("Datos del propietario.\n");
    printf("Telefono: ");
    scanf("%s", r->telefono);
    printf("Datos de la mascota.\n");
    printf("Especie: ");
    scanf("%s", r->especie);
    printf("Edad: ");
    scanf("%d", &(r->edad));
    getchar();
    printf("Cantidad de atenciones: ");
    scanf("%d", &(r->c_atenciones));
    getchar();
    char vacunas[] = "si";
    printf("Vacunas al dia (si/no): ");
    scanf("%s", vacunas);
    r->vacunas = (vacunas[0] == 's');
    printf("Siguiente control (DD/MM/YYYY): ");
    scanf("%s", r->sig_control);
    agenda = modificarRegistroAgenda(agenda, r);
  }
  return agenda;
}

Agenda *eliminarInformacionAgenda(Agenda *agenda){
  if(agenda == NULL){
    agenda = cargarArchivoAgenda(agenda);
  }
  if(agenda == NULL){
    printf("No se cargo la agenda\n");
    return NULL;
  }else{
    Registro *r = crearRegistroParaProcesar();
    agenda = borrarRegistroAgenda(agenda, r);
  }
  return agenda;
}

void menu(){
  Agenda *agenda = NULL;
  printf("Bienvenido a EL BULTO FELIZ\n\n");
  int opcion = -1;
  while(opcion != 6){
    printf("1. Cargar archivo de agenda.\n");
    printf("2. Buscar informacion de la agenda.\n");
    printf("3. Agregar informacion a la agenda.\n");
    printf("4. Modificar informacion de la agenda.\n");
    printf("5. Eliminar informacion de la agenda.\n");
    printf("6. Salir.\n");
    printf("Ingrese opcion (1-6): ");
    scanf("%d", &opcion);
    getchar();
    switch (opcion) {
      case 1:
        agenda = cargarArchivoAgenda(agenda);
        break;
      case 2:
        agenda = buscarInformacionAgenda(agenda);
        break;
      case 3:
        agenda = agregarInformacionAgenda(agenda);
        break;
      case 4:
        agenda = modificarInformacionAgenda(agenda);
        break;
      case 5:
        agenda = eliminarInformacionAgenda(agenda);
        break;
    }
  }
}

int main() {
  Agenda *agenda = NULL;
  menu(agenda);
  return 0;
}
