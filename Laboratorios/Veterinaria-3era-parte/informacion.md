## Estructura
Se recicla del Lab1 (Sacar del trabajo de Felipe)
2 estructuras:
* La estructura del nodo: Izquierda, derecha e información.
* Tipo de dato: Nombre, apellido, etc.

## Funciones solicitadas
* Buscar y modificar la información de una mascota (Lab 2)
* Agregar y eliminar registros de mascotas en el sistema (Agregar listo)
* Cargar información desde archivo Bultos.in (Reciclar desde Lab2)
* Generar un reporte a partir de un mes y año y exportar a Bultos.out

## Funciones complementarias
* Generar un árbol a partir del criterio de búsqueda del cliente
* 
