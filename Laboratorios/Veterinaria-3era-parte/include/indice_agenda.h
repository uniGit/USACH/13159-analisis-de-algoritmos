#include "stdlib.h"
#include "registro.h"

#ifndef INDICE_AGENDA
#define INDICE_AGENDA

typedef struct IndiceAgendaStruct{
  Registro *registro;
  struct IndiceAgendaStruct *izquierda;
  struct IndiceAgendaStruct *derecha;
}IndiceAgenda;

//Crea una nuevo indice de la agenda con un registro asociado
IndiceAgenda *crearIndiceAgenda(Registro *registro);

//Indica la direccion donde se debe encontrar el registro
int direccionIndiceAgendaFechaConsulta(IndiceAgenda *indice_agenda, Registro *registro);

//Inserta un nuevo registro al indice de agenda manteniendo el orden por fecha de consulta
IndiceAgenda *insertarIndiceAgenda(IndiceAgenda *indice_agenda, Registro *registro);

//Realiza un recorrido en orden del indice de agenda
IndiceAgenda *recorrerIndice(IndiceAgenda *indice_agenda, IndiceAgenda **final_recorrido, Registro *filtro);

//Borra un registro del indice
IndiceAgenda *borrarIndiceAgenda(IndiceAgenda *indice_agenda, Registro *registro);

//Modifica un registro del indice
IndiceAgenda *modificarIndiceAgenda(IndiceAgenda *indice_agenda, Registro *registro);

#endif
