#include "stdlib.h"
#include "registro.h"
#include "indice_agenda.h"

#ifndef AGENDA
#define AGENDA

  typedef struct AgendaStruct{
    IndiceAgenda *raiz_agenda;
    int total_entradas;
  }Agenda;

  //Crea una agenda a partir de un archivo de entrada
  Agenda *leerAgenda(char *archivo);

  //Crea una nueva agenda sin entradas
  Agenda *crearAgenda();

  //Agrega un nuevo registro a la agenda
  Agenda *agregarRegistroAgenda(Agenda* agenda, Registro *registro);

  //Busca los registros que coincidan con el mes y anio dados
  IndiceAgenda *buscarMesAnioAgenda(Agenda *agenda, char *mes, char *anio);

  //Borra un registro de la agenda
  Agenda *borrarRegistroAgenda(Agenda *agenda, Registro *registro);

  //Modifica un registro de la agenda
  Agenda *modificarRegistroAgenda(Agenda *agenda, Registro *registro);

#endif
