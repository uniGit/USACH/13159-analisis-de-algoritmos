#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#ifndef _REGISTRO_
#define _REGISTRO_

/**
* Representa un registro de una mascota.
* Una mascota se identifica por su nombre y el nombre del propietario
*/
typedef struct RegistroStruct{
  //Nombre del propietario
  char n_dueno[31];
  //Apellido paterno del propietario
  char ap_dueno[31];
  //Apellido materno del propietario
  char am_dueno[31];
  //Nombre de la mascota
  char n_mascota[21];
  //Especie de la mascota
  char especie[21];
  //Edad de la mascota
  int edad;
  //Telefono del propietario
  char telefono[21];
  //Cantidad de atenciones recibidas por la mascota
  int c_atenciones;
  //Indicador si la mascota posee sus vacunas al dia
  int vacunas;
  //Fecha del siguiente control de la mascota
  char sig_control[11];
}Registro;

//Crea un Registro a partir de una linea completa leida desde el archivo de carga
Registro *crearRegistro(char* linea);

//Compara dos Registros a partir de la fecha, indica cual es menor a partir de este criterio
int comparaFechaConsultaRegistro(Registro *r1, Registro *r2, int incluirDia);

//Compara dos registros a partir de los datos de identificacion
int coincideRegistro(Registro *r1, Registro *r2);

//Crea un registro con una fecha de control dada
Registro *crearRegistroFecha(char *mes, char *anio);

#endif
