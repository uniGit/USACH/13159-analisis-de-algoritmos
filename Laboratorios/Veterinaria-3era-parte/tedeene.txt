insertarIndiceAgenda
T(n) = n10c + 8c, peor caso con un arbol de maximo recorrido n
n10c es n veces verificar si moverse a izquierda o derecha (10c)
8c es insertar el nodo en el arbol
Y el O es lineal O(n) con el maximo recorrido del arbol

recorrerIndice (inorden abo)
T(n) = n(7c) + 3c hasta encontrar el primer nodo que coincida con un mes y anio, luego
n(7c) es n veces dirigirse izquierda o derecha
T(n) = 2^n * 3c + n10c
2^n es cada nivel, aumenta el doble la cantidad de nodos a verificar
3c es el recorrido del arbol nulo
n10c es crear n nodos de la lista resultante

Este es el peor caso, donde se considera un arbol donde las horas izquierdas y derechas
son las mismas en profundidad desde la raiz y la lista resultante es el arbol completo como lista
